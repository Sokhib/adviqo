# About app
This app is one part of an e-commerce app that contains only 2 screens. Build by using APIs from [api.mercadolibre.com](API.mercadolibre.com) side.

## Features of the app
- Search for a list of products matching search criteria.
- Display the results in a list with the title, price with currency, and a thumbnail.
- Inspect a particular result item in a detailed view and showed the quantity of the product, description, list of product images.
- Store a reference to the last 5 items visited while you're using them in a detailed view.

## Used libraries
  - <b>flutter_bloc:</b> used for state management
  - <b>freezed:</b> generating overriden codes for classes in order to make easy for working with classes
  - <b>connectivity_plus:</b> for detecting connection of the device
  - <b>http:</b> working with RestfulAPI
  - <b>auto_route:</b> generating navigator for pages, it saves its state of every page when we use bottom navigations easier
  - <b>flutter_staggered_grid_view:</b> in order to show the items in two columns with saving their sizes
  - <b>dartz:</b> this gives the opportunity for the function that this function can return 2 types of object. This is used for returning an error from the server or the success result
  - <b>json_serializable:</b> makes our life easier for parsing jsons

## Architecture of the app
I used the BLoC pattern for developing this app, in order to make all things simpler I used a single bloc for screens and listeners, consumers for changing the UI according to requested events. I used additional libraries for future growth of the project such as Freezed and AutoRouting.

#### You can download the app by [this](https://drive.google.com/file/d/1Wwu2SwTDep4PKaTFarSR75NHSBzQgC4l/view?usp=sharing) link
