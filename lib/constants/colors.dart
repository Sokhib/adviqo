import 'package:flutter/material.dart';

const darkBlue = Color(0xff192B58);
const transparent = Colors.transparent;
const white = Colors.white;
const black = Colors.black;
const gray = Color(0xFF7A7A7A);
const lightGray = Color(0xFFCCCCCC);
const appBarBackgorundColor = Color(0xFFF1F1F1);
