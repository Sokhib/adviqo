import 'package:freezed_annotation/freezed_annotation.dart';

part 'description.freezed.dart';
part 'description.g.dart';

@freezed
class Description with _$Description {
  const factory Description(
    dynamic text,
    String? plain_text,
    dynamic last_updated,
    dynamic date_created,
    dynamic snapshot,
  ) = _Description;

  factory Description.fromJson(Map<String, dynamic> json) => _$DescriptionFromJson(json);
}
