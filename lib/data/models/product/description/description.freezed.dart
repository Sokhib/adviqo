// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'description.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Description _$DescriptionFromJson(Map<String, dynamic> json) {
  return _Description.fromJson(json);
}

/// @nodoc
class _$DescriptionTearOff {
  const _$DescriptionTearOff();

  _Description call(dynamic text, String? plain_text, dynamic last_updated,
      dynamic date_created, dynamic snapshot) {
    return _Description(
      text,
      plain_text,
      last_updated,
      date_created,
      snapshot,
    );
  }

  Description fromJson(Map<String, Object?> json) {
    return Description.fromJson(json);
  }
}

/// @nodoc
const $Description = _$DescriptionTearOff();

/// @nodoc
mixin _$Description {
  dynamic get text => throw _privateConstructorUsedError;
  String? get plain_text => throw _privateConstructorUsedError;
  dynamic get last_updated => throw _privateConstructorUsedError;
  dynamic get date_created => throw _privateConstructorUsedError;
  dynamic get snapshot => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DescriptionCopyWith<Description> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DescriptionCopyWith<$Res> {
  factory $DescriptionCopyWith(
          Description value, $Res Function(Description) then) =
      _$DescriptionCopyWithImpl<$Res>;
  $Res call(
      {dynamic text,
      String? plain_text,
      dynamic last_updated,
      dynamic date_created,
      dynamic snapshot});
}

/// @nodoc
class _$DescriptionCopyWithImpl<$Res> implements $DescriptionCopyWith<$Res> {
  _$DescriptionCopyWithImpl(this._value, this._then);

  final Description _value;
  // ignore: unused_field
  final $Res Function(Description) _then;

  @override
  $Res call({
    Object? text = freezed,
    Object? plain_text = freezed,
    Object? last_updated = freezed,
    Object? date_created = freezed,
    Object? snapshot = freezed,
  }) {
    return _then(_value.copyWith(
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as dynamic,
      plain_text: plain_text == freezed
          ? _value.plain_text
          : plain_text // ignore: cast_nullable_to_non_nullable
              as String?,
      last_updated: last_updated == freezed
          ? _value.last_updated
          : last_updated // ignore: cast_nullable_to_non_nullable
              as dynamic,
      date_created: date_created == freezed
          ? _value.date_created
          : date_created // ignore: cast_nullable_to_non_nullable
              as dynamic,
      snapshot: snapshot == freezed
          ? _value.snapshot
          : snapshot // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
abstract class _$DescriptionCopyWith<$Res>
    implements $DescriptionCopyWith<$Res> {
  factory _$DescriptionCopyWith(
          _Description value, $Res Function(_Description) then) =
      __$DescriptionCopyWithImpl<$Res>;
  @override
  $Res call(
      {dynamic text,
      String? plain_text,
      dynamic last_updated,
      dynamic date_created,
      dynamic snapshot});
}

/// @nodoc
class __$DescriptionCopyWithImpl<$Res> extends _$DescriptionCopyWithImpl<$Res>
    implements _$DescriptionCopyWith<$Res> {
  __$DescriptionCopyWithImpl(
      _Description _value, $Res Function(_Description) _then)
      : super(_value, (v) => _then(v as _Description));

  @override
  _Description get _value => super._value as _Description;

  @override
  $Res call({
    Object? text = freezed,
    Object? plain_text = freezed,
    Object? last_updated = freezed,
    Object? date_created = freezed,
    Object? snapshot = freezed,
  }) {
    return _then(_Description(
      text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as dynamic,
      plain_text == freezed
          ? _value.plain_text
          : plain_text // ignore: cast_nullable_to_non_nullable
              as String?,
      last_updated == freezed
          ? _value.last_updated
          : last_updated // ignore: cast_nullable_to_non_nullable
              as dynamic,
      date_created == freezed
          ? _value.date_created
          : date_created // ignore: cast_nullable_to_non_nullable
              as dynamic,
      snapshot == freezed
          ? _value.snapshot
          : snapshot // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Description implements _Description {
  const _$_Description(this.text, this.plain_text, this.last_updated,
      this.date_created, this.snapshot);

  factory _$_Description.fromJson(Map<String, dynamic> json) =>
      _$$_DescriptionFromJson(json);

  @override
  final dynamic text;
  @override
  final String? plain_text;
  @override
  final dynamic last_updated;
  @override
  final dynamic date_created;
  @override
  final dynamic snapshot;

  @override
  String toString() {
    return 'Description(text: $text, plain_text: $plain_text, last_updated: $last_updated, date_created: $date_created, snapshot: $snapshot)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Description &&
            const DeepCollectionEquality().equals(other.text, text) &&
            const DeepCollectionEquality()
                .equals(other.plain_text, plain_text) &&
            const DeepCollectionEquality()
                .equals(other.last_updated, last_updated) &&
            const DeepCollectionEquality()
                .equals(other.date_created, date_created) &&
            const DeepCollectionEquality().equals(other.snapshot, snapshot));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(text),
      const DeepCollectionEquality().hash(plain_text),
      const DeepCollectionEquality().hash(last_updated),
      const DeepCollectionEquality().hash(date_created),
      const DeepCollectionEquality().hash(snapshot));

  @JsonKey(ignore: true)
  @override
  _$DescriptionCopyWith<_Description> get copyWith =>
      __$DescriptionCopyWithImpl<_Description>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DescriptionToJson(this);
  }
}

abstract class _Description implements Description {
  const factory _Description(
      dynamic text,
      String? plain_text,
      dynamic last_updated,
      dynamic date_created,
      dynamic snapshot) = _$_Description;

  factory _Description.fromJson(Map<String, dynamic> json) =
      _$_Description.fromJson;

  @override
  dynamic get text;
  @override
  String? get plain_text;
  @override
  dynamic get last_updated;
  @override
  dynamic get date_created;
  @override
  dynamic get snapshot;
  @override
  @JsonKey(ignore: true)
  _$DescriptionCopyWith<_Description> get copyWith =>
      throw _privateConstructorUsedError;
}
