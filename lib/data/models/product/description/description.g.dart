// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'description.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Description _$$_DescriptionFromJson(Map<String, dynamic> json) =>
    _$_Description(
      json['text'],
      json['plain_text'] as String?,
      json['last_updated'],
      json['date_created'],
      json['snapshot'],
    );

Map<String, dynamic> _$$_DescriptionToJson(_$_Description instance) =>
    <String, dynamic>{
      'text': instance.text,
      'plain_text': instance.plain_text,
      'last_updated': instance.last_updated,
      'date_created': instance.date_created,
      'snapshot': instance.snapshot,
    };
