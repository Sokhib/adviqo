// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'picture.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Picture _$PictureFromJson(Map<String, dynamic> json) {
  return _Picture.fromJson(json);
}

/// @nodoc
class _$PictureTearOff {
  const _$PictureTearOff();

  _Picture call(String? id, String? url, String? secureUrl, String? size,
      String? maxSize, String? quality) {
    return _Picture(
      id,
      url,
      secureUrl,
      size,
      maxSize,
      quality,
    );
  }

  Picture fromJson(Map<String, Object?> json) {
    return Picture.fromJson(json);
  }
}

/// @nodoc
const $Picture = _$PictureTearOff();

/// @nodoc
mixin _$Picture {
  String? get id => throw _privateConstructorUsedError;
  String? get url => throw _privateConstructorUsedError;
  String? get secureUrl => throw _privateConstructorUsedError;
  String? get size => throw _privateConstructorUsedError;
  String? get maxSize => throw _privateConstructorUsedError;
  String? get quality => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PictureCopyWith<Picture> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PictureCopyWith<$Res> {
  factory $PictureCopyWith(Picture value, $Res Function(Picture) then) =
      _$PictureCopyWithImpl<$Res>;
  $Res call(
      {String? id,
      String? url,
      String? secureUrl,
      String? size,
      String? maxSize,
      String? quality});
}

/// @nodoc
class _$PictureCopyWithImpl<$Res> implements $PictureCopyWith<$Res> {
  _$PictureCopyWithImpl(this._value, this._then);

  final Picture _value;
  // ignore: unused_field
  final $Res Function(Picture) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? url = freezed,
    Object? secureUrl = freezed,
    Object? size = freezed,
    Object? maxSize = freezed,
    Object? quality = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      secureUrl: secureUrl == freezed
          ? _value.secureUrl
          : secureUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      size: size == freezed
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as String?,
      maxSize: maxSize == freezed
          ? _value.maxSize
          : maxSize // ignore: cast_nullable_to_non_nullable
              as String?,
      quality: quality == freezed
          ? _value.quality
          : quality // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$PictureCopyWith<$Res> implements $PictureCopyWith<$Res> {
  factory _$PictureCopyWith(_Picture value, $Res Function(_Picture) then) =
      __$PictureCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? id,
      String? url,
      String? secureUrl,
      String? size,
      String? maxSize,
      String? quality});
}

/// @nodoc
class __$PictureCopyWithImpl<$Res> extends _$PictureCopyWithImpl<$Res>
    implements _$PictureCopyWith<$Res> {
  __$PictureCopyWithImpl(_Picture _value, $Res Function(_Picture) _then)
      : super(_value, (v) => _then(v as _Picture));

  @override
  _Picture get _value => super._value as _Picture;

  @override
  $Res call({
    Object? id = freezed,
    Object? url = freezed,
    Object? secureUrl = freezed,
    Object? size = freezed,
    Object? maxSize = freezed,
    Object? quality = freezed,
  }) {
    return _then(_Picture(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      secureUrl == freezed
          ? _value.secureUrl
          : secureUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      size == freezed
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as String?,
      maxSize == freezed
          ? _value.maxSize
          : maxSize // ignore: cast_nullable_to_non_nullable
              as String?,
      quality == freezed
          ? _value.quality
          : quality // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Picture implements _Picture {
  const _$_Picture(
      this.id, this.url, this.secureUrl, this.size, this.maxSize, this.quality);

  factory _$_Picture.fromJson(Map<String, dynamic> json) =>
      _$$_PictureFromJson(json);

  @override
  final String? id;
  @override
  final String? url;
  @override
  final String? secureUrl;
  @override
  final String? size;
  @override
  final String? maxSize;
  @override
  final String? quality;

  @override
  String toString() {
    return 'Picture(id: $id, url: $url, secureUrl: $secureUrl, size: $size, maxSize: $maxSize, quality: $quality)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Picture &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.url, url) &&
            const DeepCollectionEquality().equals(other.secureUrl, secureUrl) &&
            const DeepCollectionEquality().equals(other.size, size) &&
            const DeepCollectionEquality().equals(other.maxSize, maxSize) &&
            const DeepCollectionEquality().equals(other.quality, quality));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(url),
      const DeepCollectionEquality().hash(secureUrl),
      const DeepCollectionEquality().hash(size),
      const DeepCollectionEquality().hash(maxSize),
      const DeepCollectionEquality().hash(quality));

  @JsonKey(ignore: true)
  @override
  _$PictureCopyWith<_Picture> get copyWith =>
      __$PictureCopyWithImpl<_Picture>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PictureToJson(this);
  }
}

abstract class _Picture implements Picture {
  const factory _Picture(String? id, String? url, String? secureUrl,
      String? size, String? maxSize, String? quality) = _$_Picture;

  factory _Picture.fromJson(Map<String, dynamic> json) = _$_Picture.fromJson;

  @override
  String? get id;
  @override
  String? get url;
  @override
  String? get secureUrl;
  @override
  String? get size;
  @override
  String? get maxSize;
  @override
  String? get quality;
  @override
  @JsonKey(ignore: true)
  _$PictureCopyWith<_Picture> get copyWith =>
      throw _privateConstructorUsedError;
}
