// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'picture.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Picture _$$_PictureFromJson(Map<String, dynamic> json) => _$_Picture(
      json['id'] as String?,
      json['url'] as String?,
      json['secureUrl'] as String?,
      json['size'] as String?,
      json['maxSize'] as String?,
      json['quality'] as String?,
    );

Map<String, dynamic> _$$_PictureToJson(_$_Picture instance) =>
    <String, dynamic>{
      'id': instance.id,
      'url': instance.url,
      'secureUrl': instance.secureUrl,
      'size': instance.size,
      'maxSize': instance.maxSize,
      'quality': instance.quality,
    };
