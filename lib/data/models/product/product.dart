import 'package:adviqo/data/models/product/pictures/picture.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'product.freezed.dart';
part 'product.g.dart';

@freezed
class Product with _$Product {
  const factory Product(
    String? id,
    dynamic siteId,
    String? title,
    dynamic subtitle,
    double? sellerId,
    dynamic categoryId,
    dynamic officialStoreId,
    double? price,
    double? basePrice,
    dynamic originalPrice,
    dynamic currencyId,
    int? initial_quantity,
    int? available_quantity,
    int? sold_quantity,
    List<dynamic>? saleTerms,
    dynamic buyingMode,
    dynamic listingTypeId,
    dynamic startTime,
    dynamic stopTime,
    dynamic condition,
    dynamic permalink,
    dynamic thumbnailId,
    dynamic thumbnail,
    dynamic secureThumbnail,
    List<Picture>? pictures,
    dynamic videoId,
    List<dynamic>? descriptions,
    dynamic acceptsMercadopago,
    List<dynamic>? nonMercadoPagoPaymentMethods,
    dynamic shipping,
    dynamic doubleernationalDeliveryMode,
    dynamic sellerAddress,
    dynamic sellerContact,
    dynamic location,
    List<dynamic>? coverageAreas,
    List<dynamic>? attributes,
    List<dynamic>? warnings,
    dynamic listingSource,
    List<dynamic>? variations,
    dynamic status,
    List<dynamic>? subStatus,
    List<String>? tags,
    String? warranty,
    dynamic catalogProductId,
    dynamic domainId,
    dynamic parentItemId,
    dynamic differentialPricing,
    List<dynamic>? dealIds,
    bool? automaticRelist,
    dynamic dateCreated,
    dynamic lastUpdated,
    dynamic health,
    dynamic catalogListing,
    List<dynamic>? channels,
  ) = _Product;

  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);
}
