// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'product.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Product _$ProductFromJson(Map<String, dynamic> json) {
  return _Product.fromJson(json);
}

/// @nodoc
class _$ProductTearOff {
  const _$ProductTearOff();

  _Product call(
      String? id,
      dynamic siteId,
      String? title,
      dynamic subtitle,
      double? sellerId,
      dynamic categoryId,
      dynamic officialStoreId,
      double? price,
      double? basePrice,
      dynamic originalPrice,
      dynamic currencyId,
      int? initial_quantity,
      int? available_quantity,
      int? sold_quantity,
      List<dynamic>? saleTerms,
      dynamic buyingMode,
      dynamic listingTypeId,
      dynamic startTime,
      dynamic stopTime,
      dynamic condition,
      dynamic permalink,
      dynamic thumbnailId,
      dynamic thumbnail,
      dynamic secureThumbnail,
      List<Picture>? pictures,
      dynamic videoId,
      List<dynamic>? descriptions,
      dynamic acceptsMercadopago,
      List<dynamic>? nonMercadoPagoPaymentMethods,
      dynamic shipping,
      dynamic doubleernationalDeliveryMode,
      dynamic sellerAddress,
      dynamic sellerContact,
      dynamic location,
      List<dynamic>? coverageAreas,
      List<dynamic>? attributes,
      List<dynamic>? warnings,
      dynamic listingSource,
      List<dynamic>? variations,
      dynamic status,
      List<dynamic>? subStatus,
      List<String>? tags,
      String? warranty,
      dynamic catalogProductId,
      dynamic domainId,
      dynamic parentItemId,
      dynamic differentialPricing,
      List<dynamic>? dealIds,
      bool? automaticRelist,
      dynamic dateCreated,
      dynamic lastUpdated,
      dynamic health,
      dynamic catalogListing,
      List<dynamic>? channels) {
    return _Product(
      id,
      siteId,
      title,
      subtitle,
      sellerId,
      categoryId,
      officialStoreId,
      price,
      basePrice,
      originalPrice,
      currencyId,
      initial_quantity,
      available_quantity,
      sold_quantity,
      saleTerms,
      buyingMode,
      listingTypeId,
      startTime,
      stopTime,
      condition,
      permalink,
      thumbnailId,
      thumbnail,
      secureThumbnail,
      pictures,
      videoId,
      descriptions,
      acceptsMercadopago,
      nonMercadoPagoPaymentMethods,
      shipping,
      doubleernationalDeliveryMode,
      sellerAddress,
      sellerContact,
      location,
      coverageAreas,
      attributes,
      warnings,
      listingSource,
      variations,
      status,
      subStatus,
      tags,
      warranty,
      catalogProductId,
      domainId,
      parentItemId,
      differentialPricing,
      dealIds,
      automaticRelist,
      dateCreated,
      lastUpdated,
      health,
      catalogListing,
      channels,
    );
  }

  Product fromJson(Map<String, Object?> json) {
    return Product.fromJson(json);
  }
}

/// @nodoc
const $Product = _$ProductTearOff();

/// @nodoc
mixin _$Product {
  String? get id => throw _privateConstructorUsedError;
  dynamic get siteId => throw _privateConstructorUsedError;
  String? get title => throw _privateConstructorUsedError;
  dynamic get subtitle => throw _privateConstructorUsedError;
  double? get sellerId => throw _privateConstructorUsedError;
  dynamic get categoryId => throw _privateConstructorUsedError;
  dynamic get officialStoreId => throw _privateConstructorUsedError;
  double? get price => throw _privateConstructorUsedError;
  double? get basePrice => throw _privateConstructorUsedError;
  dynamic get originalPrice => throw _privateConstructorUsedError;
  dynamic get currencyId => throw _privateConstructorUsedError;
  int? get initial_quantity => throw _privateConstructorUsedError;
  int? get available_quantity => throw _privateConstructorUsedError;
  int? get sold_quantity => throw _privateConstructorUsedError;
  List<dynamic>? get saleTerms => throw _privateConstructorUsedError;
  dynamic get buyingMode => throw _privateConstructorUsedError;
  dynamic get listingTypeId => throw _privateConstructorUsedError;
  dynamic get startTime => throw _privateConstructorUsedError;
  dynamic get stopTime => throw _privateConstructorUsedError;
  dynamic get condition => throw _privateConstructorUsedError;
  dynamic get permalink => throw _privateConstructorUsedError;
  dynamic get thumbnailId => throw _privateConstructorUsedError;
  dynamic get thumbnail => throw _privateConstructorUsedError;
  dynamic get secureThumbnail => throw _privateConstructorUsedError;
  List<Picture>? get pictures => throw _privateConstructorUsedError;
  dynamic get videoId => throw _privateConstructorUsedError;
  List<dynamic>? get descriptions => throw _privateConstructorUsedError;
  dynamic get acceptsMercadopago => throw _privateConstructorUsedError;
  List<dynamic>? get nonMercadoPagoPaymentMethods =>
      throw _privateConstructorUsedError;
  dynamic get shipping => throw _privateConstructorUsedError;
  dynamic get doubleernationalDeliveryMode =>
      throw _privateConstructorUsedError;
  dynamic get sellerAddress => throw _privateConstructorUsedError;
  dynamic get sellerContact => throw _privateConstructorUsedError;
  dynamic get location => throw _privateConstructorUsedError;
  List<dynamic>? get coverageAreas => throw _privateConstructorUsedError;
  List<dynamic>? get attributes => throw _privateConstructorUsedError;
  List<dynamic>? get warnings => throw _privateConstructorUsedError;
  dynamic get listingSource => throw _privateConstructorUsedError;
  List<dynamic>? get variations => throw _privateConstructorUsedError;
  dynamic get status => throw _privateConstructorUsedError;
  List<dynamic>? get subStatus => throw _privateConstructorUsedError;
  List<String>? get tags => throw _privateConstructorUsedError;
  String? get warranty => throw _privateConstructorUsedError;
  dynamic get catalogProductId => throw _privateConstructorUsedError;
  dynamic get domainId => throw _privateConstructorUsedError;
  dynamic get parentItemId => throw _privateConstructorUsedError;
  dynamic get differentialPricing => throw _privateConstructorUsedError;
  List<dynamic>? get dealIds => throw _privateConstructorUsedError;
  bool? get automaticRelist => throw _privateConstructorUsedError;
  dynamic get dateCreated => throw _privateConstructorUsedError;
  dynamic get lastUpdated => throw _privateConstructorUsedError;
  dynamic get health => throw _privateConstructorUsedError;
  dynamic get catalogListing => throw _privateConstructorUsedError;
  List<dynamic>? get channels => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductCopyWith<Product> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductCopyWith<$Res> {
  factory $ProductCopyWith(Product value, $Res Function(Product) then) =
      _$ProductCopyWithImpl<$Res>;
  $Res call(
      {String? id,
      dynamic siteId,
      String? title,
      dynamic subtitle,
      double? sellerId,
      dynamic categoryId,
      dynamic officialStoreId,
      double? price,
      double? basePrice,
      dynamic originalPrice,
      dynamic currencyId,
      int? initial_quantity,
      int? available_quantity,
      int? sold_quantity,
      List<dynamic>? saleTerms,
      dynamic buyingMode,
      dynamic listingTypeId,
      dynamic startTime,
      dynamic stopTime,
      dynamic condition,
      dynamic permalink,
      dynamic thumbnailId,
      dynamic thumbnail,
      dynamic secureThumbnail,
      List<Picture>? pictures,
      dynamic videoId,
      List<dynamic>? descriptions,
      dynamic acceptsMercadopago,
      List<dynamic>? nonMercadoPagoPaymentMethods,
      dynamic shipping,
      dynamic doubleernationalDeliveryMode,
      dynamic sellerAddress,
      dynamic sellerContact,
      dynamic location,
      List<dynamic>? coverageAreas,
      List<dynamic>? attributes,
      List<dynamic>? warnings,
      dynamic listingSource,
      List<dynamic>? variations,
      dynamic status,
      List<dynamic>? subStatus,
      List<String>? tags,
      String? warranty,
      dynamic catalogProductId,
      dynamic domainId,
      dynamic parentItemId,
      dynamic differentialPricing,
      List<dynamic>? dealIds,
      bool? automaticRelist,
      dynamic dateCreated,
      dynamic lastUpdated,
      dynamic health,
      dynamic catalogListing,
      List<dynamic>? channels});
}

/// @nodoc
class _$ProductCopyWithImpl<$Res> implements $ProductCopyWith<$Res> {
  _$ProductCopyWithImpl(this._value, this._then);

  final Product _value;
  // ignore: unused_field
  final $Res Function(Product) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? siteId = freezed,
    Object? title = freezed,
    Object? subtitle = freezed,
    Object? sellerId = freezed,
    Object? categoryId = freezed,
    Object? officialStoreId = freezed,
    Object? price = freezed,
    Object? basePrice = freezed,
    Object? originalPrice = freezed,
    Object? currencyId = freezed,
    Object? initial_quantity = freezed,
    Object? available_quantity = freezed,
    Object? sold_quantity = freezed,
    Object? saleTerms = freezed,
    Object? buyingMode = freezed,
    Object? listingTypeId = freezed,
    Object? startTime = freezed,
    Object? stopTime = freezed,
    Object? condition = freezed,
    Object? permalink = freezed,
    Object? thumbnailId = freezed,
    Object? thumbnail = freezed,
    Object? secureThumbnail = freezed,
    Object? pictures = freezed,
    Object? videoId = freezed,
    Object? descriptions = freezed,
    Object? acceptsMercadopago = freezed,
    Object? nonMercadoPagoPaymentMethods = freezed,
    Object? shipping = freezed,
    Object? doubleernationalDeliveryMode = freezed,
    Object? sellerAddress = freezed,
    Object? sellerContact = freezed,
    Object? location = freezed,
    Object? coverageAreas = freezed,
    Object? attributes = freezed,
    Object? warnings = freezed,
    Object? listingSource = freezed,
    Object? variations = freezed,
    Object? status = freezed,
    Object? subStatus = freezed,
    Object? tags = freezed,
    Object? warranty = freezed,
    Object? catalogProductId = freezed,
    Object? domainId = freezed,
    Object? parentItemId = freezed,
    Object? differentialPricing = freezed,
    Object? dealIds = freezed,
    Object? automaticRelist = freezed,
    Object? dateCreated = freezed,
    Object? lastUpdated = freezed,
    Object? health = freezed,
    Object? catalogListing = freezed,
    Object? channels = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      siteId: siteId == freezed
          ? _value.siteId
          : siteId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      subtitle: subtitle == freezed
          ? _value.subtitle
          : subtitle // ignore: cast_nullable_to_non_nullable
              as dynamic,
      sellerId: sellerId == freezed
          ? _value.sellerId
          : sellerId // ignore: cast_nullable_to_non_nullable
              as double?,
      categoryId: categoryId == freezed
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      officialStoreId: officialStoreId == freezed
          ? _value.officialStoreId
          : officialStoreId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double?,
      basePrice: basePrice == freezed
          ? _value.basePrice
          : basePrice // ignore: cast_nullable_to_non_nullable
              as double?,
      originalPrice: originalPrice == freezed
          ? _value.originalPrice
          : originalPrice // ignore: cast_nullable_to_non_nullable
              as dynamic,
      currencyId: currencyId == freezed
          ? _value.currencyId
          : currencyId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      initial_quantity: initial_quantity == freezed
          ? _value.initial_quantity
          : initial_quantity // ignore: cast_nullable_to_non_nullable
              as int?,
      available_quantity: available_quantity == freezed
          ? _value.available_quantity
          : available_quantity // ignore: cast_nullable_to_non_nullable
              as int?,
      sold_quantity: sold_quantity == freezed
          ? _value.sold_quantity
          : sold_quantity // ignore: cast_nullable_to_non_nullable
              as int?,
      saleTerms: saleTerms == freezed
          ? _value.saleTerms
          : saleTerms // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      buyingMode: buyingMode == freezed
          ? _value.buyingMode
          : buyingMode // ignore: cast_nullable_to_non_nullable
              as dynamic,
      listingTypeId: listingTypeId == freezed
          ? _value.listingTypeId
          : listingTypeId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      startTime: startTime == freezed
          ? _value.startTime
          : startTime // ignore: cast_nullable_to_non_nullable
              as dynamic,
      stopTime: stopTime == freezed
          ? _value.stopTime
          : stopTime // ignore: cast_nullable_to_non_nullable
              as dynamic,
      condition: condition == freezed
          ? _value.condition
          : condition // ignore: cast_nullable_to_non_nullable
              as dynamic,
      permalink: permalink == freezed
          ? _value.permalink
          : permalink // ignore: cast_nullable_to_non_nullable
              as dynamic,
      thumbnailId: thumbnailId == freezed
          ? _value.thumbnailId
          : thumbnailId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      thumbnail: thumbnail == freezed
          ? _value.thumbnail
          : thumbnail // ignore: cast_nullable_to_non_nullable
              as dynamic,
      secureThumbnail: secureThumbnail == freezed
          ? _value.secureThumbnail
          : secureThumbnail // ignore: cast_nullable_to_non_nullable
              as dynamic,
      pictures: pictures == freezed
          ? _value.pictures
          : pictures // ignore: cast_nullable_to_non_nullable
              as List<Picture>?,
      videoId: videoId == freezed
          ? _value.videoId
          : videoId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      descriptions: descriptions == freezed
          ? _value.descriptions
          : descriptions // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      acceptsMercadopago: acceptsMercadopago == freezed
          ? _value.acceptsMercadopago
          : acceptsMercadopago // ignore: cast_nullable_to_non_nullable
              as dynamic,
      nonMercadoPagoPaymentMethods: nonMercadoPagoPaymentMethods == freezed
          ? _value.nonMercadoPagoPaymentMethods
          : nonMercadoPagoPaymentMethods // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      shipping: shipping == freezed
          ? _value.shipping
          : shipping // ignore: cast_nullable_to_non_nullable
              as dynamic,
      doubleernationalDeliveryMode: doubleernationalDeliveryMode == freezed
          ? _value.doubleernationalDeliveryMode
          : doubleernationalDeliveryMode // ignore: cast_nullable_to_non_nullable
              as dynamic,
      sellerAddress: sellerAddress == freezed
          ? _value.sellerAddress
          : sellerAddress // ignore: cast_nullable_to_non_nullable
              as dynamic,
      sellerContact: sellerContact == freezed
          ? _value.sellerContact
          : sellerContact // ignore: cast_nullable_to_non_nullable
              as dynamic,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as dynamic,
      coverageAreas: coverageAreas == freezed
          ? _value.coverageAreas
          : coverageAreas // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      attributes: attributes == freezed
          ? _value.attributes
          : attributes // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      warnings: warnings == freezed
          ? _value.warnings
          : warnings // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      listingSource: listingSource == freezed
          ? _value.listingSource
          : listingSource // ignore: cast_nullable_to_non_nullable
              as dynamic,
      variations: variations == freezed
          ? _value.variations
          : variations // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as dynamic,
      subStatus: subStatus == freezed
          ? _value.subStatus
          : subStatus // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      tags: tags == freezed
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      warranty: warranty == freezed
          ? _value.warranty
          : warranty // ignore: cast_nullable_to_non_nullable
              as String?,
      catalogProductId: catalogProductId == freezed
          ? _value.catalogProductId
          : catalogProductId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      domainId: domainId == freezed
          ? _value.domainId
          : domainId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      parentItemId: parentItemId == freezed
          ? _value.parentItemId
          : parentItemId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      differentialPricing: differentialPricing == freezed
          ? _value.differentialPricing
          : differentialPricing // ignore: cast_nullable_to_non_nullable
              as dynamic,
      dealIds: dealIds == freezed
          ? _value.dealIds
          : dealIds // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      automaticRelist: automaticRelist == freezed
          ? _value.automaticRelist
          : automaticRelist // ignore: cast_nullable_to_non_nullable
              as bool?,
      dateCreated: dateCreated == freezed
          ? _value.dateCreated
          : dateCreated // ignore: cast_nullable_to_non_nullable
              as dynamic,
      lastUpdated: lastUpdated == freezed
          ? _value.lastUpdated
          : lastUpdated // ignore: cast_nullable_to_non_nullable
              as dynamic,
      health: health == freezed
          ? _value.health
          : health // ignore: cast_nullable_to_non_nullable
              as dynamic,
      catalogListing: catalogListing == freezed
          ? _value.catalogListing
          : catalogListing // ignore: cast_nullable_to_non_nullable
              as dynamic,
      channels: channels == freezed
          ? _value.channels
          : channels // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
    ));
  }
}

/// @nodoc
abstract class _$ProductCopyWith<$Res> implements $ProductCopyWith<$Res> {
  factory _$ProductCopyWith(_Product value, $Res Function(_Product) then) =
      __$ProductCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? id,
      dynamic siteId,
      String? title,
      dynamic subtitle,
      double? sellerId,
      dynamic categoryId,
      dynamic officialStoreId,
      double? price,
      double? basePrice,
      dynamic originalPrice,
      dynamic currencyId,
      int? initial_quantity,
      int? available_quantity,
      int? sold_quantity,
      List<dynamic>? saleTerms,
      dynamic buyingMode,
      dynamic listingTypeId,
      dynamic startTime,
      dynamic stopTime,
      dynamic condition,
      dynamic permalink,
      dynamic thumbnailId,
      dynamic thumbnail,
      dynamic secureThumbnail,
      List<Picture>? pictures,
      dynamic videoId,
      List<dynamic>? descriptions,
      dynamic acceptsMercadopago,
      List<dynamic>? nonMercadoPagoPaymentMethods,
      dynamic shipping,
      dynamic doubleernationalDeliveryMode,
      dynamic sellerAddress,
      dynamic sellerContact,
      dynamic location,
      List<dynamic>? coverageAreas,
      List<dynamic>? attributes,
      List<dynamic>? warnings,
      dynamic listingSource,
      List<dynamic>? variations,
      dynamic status,
      List<dynamic>? subStatus,
      List<String>? tags,
      String? warranty,
      dynamic catalogProductId,
      dynamic domainId,
      dynamic parentItemId,
      dynamic differentialPricing,
      List<dynamic>? dealIds,
      bool? automaticRelist,
      dynamic dateCreated,
      dynamic lastUpdated,
      dynamic health,
      dynamic catalogListing,
      List<dynamic>? channels});
}

/// @nodoc
class __$ProductCopyWithImpl<$Res> extends _$ProductCopyWithImpl<$Res>
    implements _$ProductCopyWith<$Res> {
  __$ProductCopyWithImpl(_Product _value, $Res Function(_Product) _then)
      : super(_value, (v) => _then(v as _Product));

  @override
  _Product get _value => super._value as _Product;

  @override
  $Res call({
    Object? id = freezed,
    Object? siteId = freezed,
    Object? title = freezed,
    Object? subtitle = freezed,
    Object? sellerId = freezed,
    Object? categoryId = freezed,
    Object? officialStoreId = freezed,
    Object? price = freezed,
    Object? basePrice = freezed,
    Object? originalPrice = freezed,
    Object? currencyId = freezed,
    Object? initial_quantity = freezed,
    Object? available_quantity = freezed,
    Object? sold_quantity = freezed,
    Object? saleTerms = freezed,
    Object? buyingMode = freezed,
    Object? listingTypeId = freezed,
    Object? startTime = freezed,
    Object? stopTime = freezed,
    Object? condition = freezed,
    Object? permalink = freezed,
    Object? thumbnailId = freezed,
    Object? thumbnail = freezed,
    Object? secureThumbnail = freezed,
    Object? pictures = freezed,
    Object? videoId = freezed,
    Object? descriptions = freezed,
    Object? acceptsMercadopago = freezed,
    Object? nonMercadoPagoPaymentMethods = freezed,
    Object? shipping = freezed,
    Object? doubleernationalDeliveryMode = freezed,
    Object? sellerAddress = freezed,
    Object? sellerContact = freezed,
    Object? location = freezed,
    Object? coverageAreas = freezed,
    Object? attributes = freezed,
    Object? warnings = freezed,
    Object? listingSource = freezed,
    Object? variations = freezed,
    Object? status = freezed,
    Object? subStatus = freezed,
    Object? tags = freezed,
    Object? warranty = freezed,
    Object? catalogProductId = freezed,
    Object? domainId = freezed,
    Object? parentItemId = freezed,
    Object? differentialPricing = freezed,
    Object? dealIds = freezed,
    Object? automaticRelist = freezed,
    Object? dateCreated = freezed,
    Object? lastUpdated = freezed,
    Object? health = freezed,
    Object? catalogListing = freezed,
    Object? channels = freezed,
  }) {
    return _then(_Product(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      siteId == freezed
          ? _value.siteId
          : siteId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      subtitle == freezed
          ? _value.subtitle
          : subtitle // ignore: cast_nullable_to_non_nullable
              as dynamic,
      sellerId == freezed
          ? _value.sellerId
          : sellerId // ignore: cast_nullable_to_non_nullable
              as double?,
      categoryId == freezed
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      officialStoreId == freezed
          ? _value.officialStoreId
          : officialStoreId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double?,
      basePrice == freezed
          ? _value.basePrice
          : basePrice // ignore: cast_nullable_to_non_nullable
              as double?,
      originalPrice == freezed
          ? _value.originalPrice
          : originalPrice // ignore: cast_nullable_to_non_nullable
              as dynamic,
      currencyId == freezed
          ? _value.currencyId
          : currencyId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      initial_quantity == freezed
          ? _value.initial_quantity
          : initial_quantity // ignore: cast_nullable_to_non_nullable
              as int?,
      available_quantity == freezed
          ? _value.available_quantity
          : available_quantity // ignore: cast_nullable_to_non_nullable
              as int?,
      sold_quantity == freezed
          ? _value.sold_quantity
          : sold_quantity // ignore: cast_nullable_to_non_nullable
              as int?,
      saleTerms == freezed
          ? _value.saleTerms
          : saleTerms // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      buyingMode == freezed
          ? _value.buyingMode
          : buyingMode // ignore: cast_nullable_to_non_nullable
              as dynamic,
      listingTypeId == freezed
          ? _value.listingTypeId
          : listingTypeId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      startTime == freezed
          ? _value.startTime
          : startTime // ignore: cast_nullable_to_non_nullable
              as dynamic,
      stopTime == freezed
          ? _value.stopTime
          : stopTime // ignore: cast_nullable_to_non_nullable
              as dynamic,
      condition == freezed
          ? _value.condition
          : condition // ignore: cast_nullable_to_non_nullable
              as dynamic,
      permalink == freezed
          ? _value.permalink
          : permalink // ignore: cast_nullable_to_non_nullable
              as dynamic,
      thumbnailId == freezed
          ? _value.thumbnailId
          : thumbnailId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      thumbnail == freezed
          ? _value.thumbnail
          : thumbnail // ignore: cast_nullable_to_non_nullable
              as dynamic,
      secureThumbnail == freezed
          ? _value.secureThumbnail
          : secureThumbnail // ignore: cast_nullable_to_non_nullable
              as dynamic,
      pictures == freezed
          ? _value.pictures
          : pictures // ignore: cast_nullable_to_non_nullable
              as List<Picture>?,
      videoId == freezed
          ? _value.videoId
          : videoId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      descriptions == freezed
          ? _value.descriptions
          : descriptions // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      acceptsMercadopago == freezed
          ? _value.acceptsMercadopago
          : acceptsMercadopago // ignore: cast_nullable_to_non_nullable
              as dynamic,
      nonMercadoPagoPaymentMethods == freezed
          ? _value.nonMercadoPagoPaymentMethods
          : nonMercadoPagoPaymentMethods // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      shipping == freezed
          ? _value.shipping
          : shipping // ignore: cast_nullable_to_non_nullable
              as dynamic,
      doubleernationalDeliveryMode == freezed
          ? _value.doubleernationalDeliveryMode
          : doubleernationalDeliveryMode // ignore: cast_nullable_to_non_nullable
              as dynamic,
      sellerAddress == freezed
          ? _value.sellerAddress
          : sellerAddress // ignore: cast_nullable_to_non_nullable
              as dynamic,
      sellerContact == freezed
          ? _value.sellerContact
          : sellerContact // ignore: cast_nullable_to_non_nullable
              as dynamic,
      location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as dynamic,
      coverageAreas == freezed
          ? _value.coverageAreas
          : coverageAreas // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      attributes == freezed
          ? _value.attributes
          : attributes // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      warnings == freezed
          ? _value.warnings
          : warnings // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      listingSource == freezed
          ? _value.listingSource
          : listingSource // ignore: cast_nullable_to_non_nullable
              as dynamic,
      variations == freezed
          ? _value.variations
          : variations // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as dynamic,
      subStatus == freezed
          ? _value.subStatus
          : subStatus // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      tags == freezed
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      warranty == freezed
          ? _value.warranty
          : warranty // ignore: cast_nullable_to_non_nullable
              as String?,
      catalogProductId == freezed
          ? _value.catalogProductId
          : catalogProductId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      domainId == freezed
          ? _value.domainId
          : domainId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      parentItemId == freezed
          ? _value.parentItemId
          : parentItemId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      differentialPricing == freezed
          ? _value.differentialPricing
          : differentialPricing // ignore: cast_nullable_to_non_nullable
              as dynamic,
      dealIds == freezed
          ? _value.dealIds
          : dealIds // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      automaticRelist == freezed
          ? _value.automaticRelist
          : automaticRelist // ignore: cast_nullable_to_non_nullable
              as bool?,
      dateCreated == freezed
          ? _value.dateCreated
          : dateCreated // ignore: cast_nullable_to_non_nullable
              as dynamic,
      lastUpdated == freezed
          ? _value.lastUpdated
          : lastUpdated // ignore: cast_nullable_to_non_nullable
              as dynamic,
      health == freezed
          ? _value.health
          : health // ignore: cast_nullable_to_non_nullable
              as dynamic,
      catalogListing == freezed
          ? _value.catalogListing
          : catalogListing // ignore: cast_nullable_to_non_nullable
              as dynamic,
      channels == freezed
          ? _value.channels
          : channels // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Product implements _Product {
  const _$_Product(
      this.id,
      this.siteId,
      this.title,
      this.subtitle,
      this.sellerId,
      this.categoryId,
      this.officialStoreId,
      this.price,
      this.basePrice,
      this.originalPrice,
      this.currencyId,
      this.initial_quantity,
      this.available_quantity,
      this.sold_quantity,
      this.saleTerms,
      this.buyingMode,
      this.listingTypeId,
      this.startTime,
      this.stopTime,
      this.condition,
      this.permalink,
      this.thumbnailId,
      this.thumbnail,
      this.secureThumbnail,
      this.pictures,
      this.videoId,
      this.descriptions,
      this.acceptsMercadopago,
      this.nonMercadoPagoPaymentMethods,
      this.shipping,
      this.doubleernationalDeliveryMode,
      this.sellerAddress,
      this.sellerContact,
      this.location,
      this.coverageAreas,
      this.attributes,
      this.warnings,
      this.listingSource,
      this.variations,
      this.status,
      this.subStatus,
      this.tags,
      this.warranty,
      this.catalogProductId,
      this.domainId,
      this.parentItemId,
      this.differentialPricing,
      this.dealIds,
      this.automaticRelist,
      this.dateCreated,
      this.lastUpdated,
      this.health,
      this.catalogListing,
      this.channels);

  factory _$_Product.fromJson(Map<String, dynamic> json) =>
      _$$_ProductFromJson(json);

  @override
  final String? id;
  @override
  final dynamic siteId;
  @override
  final String? title;
  @override
  final dynamic subtitle;
  @override
  final double? sellerId;
  @override
  final dynamic categoryId;
  @override
  final dynamic officialStoreId;
  @override
  final double? price;
  @override
  final double? basePrice;
  @override
  final dynamic originalPrice;
  @override
  final dynamic currencyId;
  @override
  final int? initial_quantity;
  @override
  final int? available_quantity;
  @override
  final int? sold_quantity;
  @override
  final List<dynamic>? saleTerms;
  @override
  final dynamic buyingMode;
  @override
  final dynamic listingTypeId;
  @override
  final dynamic startTime;
  @override
  final dynamic stopTime;
  @override
  final dynamic condition;
  @override
  final dynamic permalink;
  @override
  final dynamic thumbnailId;
  @override
  final dynamic thumbnail;
  @override
  final dynamic secureThumbnail;
  @override
  final List<Picture>? pictures;
  @override
  final dynamic videoId;
  @override
  final List<dynamic>? descriptions;
  @override
  final dynamic acceptsMercadopago;
  @override
  final List<dynamic>? nonMercadoPagoPaymentMethods;
  @override
  final dynamic shipping;
  @override
  final dynamic doubleernationalDeliveryMode;
  @override
  final dynamic sellerAddress;
  @override
  final dynamic sellerContact;
  @override
  final dynamic location;
  @override
  final List<dynamic>? coverageAreas;
  @override
  final List<dynamic>? attributes;
  @override
  final List<dynamic>? warnings;
  @override
  final dynamic listingSource;
  @override
  final List<dynamic>? variations;
  @override
  final dynamic status;
  @override
  final List<dynamic>? subStatus;
  @override
  final List<String>? tags;
  @override
  final String? warranty;
  @override
  final dynamic catalogProductId;
  @override
  final dynamic domainId;
  @override
  final dynamic parentItemId;
  @override
  final dynamic differentialPricing;
  @override
  final List<dynamic>? dealIds;
  @override
  final bool? automaticRelist;
  @override
  final dynamic dateCreated;
  @override
  final dynamic lastUpdated;
  @override
  final dynamic health;
  @override
  final dynamic catalogListing;
  @override
  final List<dynamic>? channels;

  @override
  String toString() {
    return 'Product(id: $id, siteId: $siteId, title: $title, subtitle: $subtitle, sellerId: $sellerId, categoryId: $categoryId, officialStoreId: $officialStoreId, price: $price, basePrice: $basePrice, originalPrice: $originalPrice, currencyId: $currencyId, initial_quantity: $initial_quantity, available_quantity: $available_quantity, sold_quantity: $sold_quantity, saleTerms: $saleTerms, buyingMode: $buyingMode, listingTypeId: $listingTypeId, startTime: $startTime, stopTime: $stopTime, condition: $condition, permalink: $permalink, thumbnailId: $thumbnailId, thumbnail: $thumbnail, secureThumbnail: $secureThumbnail, pictures: $pictures, videoId: $videoId, descriptions: $descriptions, acceptsMercadopago: $acceptsMercadopago, nonMercadoPagoPaymentMethods: $nonMercadoPagoPaymentMethods, shipping: $shipping, doubleernationalDeliveryMode: $doubleernationalDeliveryMode, sellerAddress: $sellerAddress, sellerContact: $sellerContact, location: $location, coverageAreas: $coverageAreas, attributes: $attributes, warnings: $warnings, listingSource: $listingSource, variations: $variations, status: $status, subStatus: $subStatus, tags: $tags, warranty: $warranty, catalogProductId: $catalogProductId, domainId: $domainId, parentItemId: $parentItemId, differentialPricing: $differentialPricing, dealIds: $dealIds, automaticRelist: $automaticRelist, dateCreated: $dateCreated, lastUpdated: $lastUpdated, health: $health, catalogListing: $catalogListing, channels: $channels)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Product &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.siteId, siteId) &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.subtitle, subtitle) &&
            const DeepCollectionEquality().equals(other.sellerId, sellerId) &&
            const DeepCollectionEquality()
                .equals(other.categoryId, categoryId) &&
            const DeepCollectionEquality()
                .equals(other.officialStoreId, officialStoreId) &&
            const DeepCollectionEquality().equals(other.price, price) &&
            const DeepCollectionEquality().equals(other.basePrice, basePrice) &&
            const DeepCollectionEquality()
                .equals(other.originalPrice, originalPrice) &&
            const DeepCollectionEquality()
                .equals(other.currencyId, currencyId) &&
            const DeepCollectionEquality()
                .equals(other.initial_quantity, initial_quantity) &&
            const DeepCollectionEquality()
                .equals(other.available_quantity, available_quantity) &&
            const DeepCollectionEquality()
                .equals(other.sold_quantity, sold_quantity) &&
            const DeepCollectionEquality().equals(other.saleTerms, saleTerms) &&
            const DeepCollectionEquality()
                .equals(other.buyingMode, buyingMode) &&
            const DeepCollectionEquality()
                .equals(other.listingTypeId, listingTypeId) &&
            const DeepCollectionEquality().equals(other.startTime, startTime) &&
            const DeepCollectionEquality().equals(other.stopTime, stopTime) &&
            const DeepCollectionEquality().equals(other.condition, condition) &&
            const DeepCollectionEquality().equals(other.permalink, permalink) &&
            const DeepCollectionEquality()
                .equals(other.thumbnailId, thumbnailId) &&
            const DeepCollectionEquality().equals(other.thumbnail, thumbnail) &&
            const DeepCollectionEquality()
                .equals(other.secureThumbnail, secureThumbnail) &&
            const DeepCollectionEquality().equals(other.pictures, pictures) &&
            const DeepCollectionEquality().equals(other.videoId, videoId) &&
            const DeepCollectionEquality()
                .equals(other.descriptions, descriptions) &&
            const DeepCollectionEquality()
                .equals(other.acceptsMercadopago, acceptsMercadopago) &&
            const DeepCollectionEquality().equals(
                other.nonMercadoPagoPaymentMethods,
                nonMercadoPagoPaymentMethods) &&
            const DeepCollectionEquality().equals(other.shipping, shipping) &&
            const DeepCollectionEquality().equals(
                other.doubleernationalDeliveryMode,
                doubleernationalDeliveryMode) &&
            const DeepCollectionEquality()
                .equals(other.sellerAddress, sellerAddress) &&
            const DeepCollectionEquality()
                .equals(other.sellerContact, sellerContact) &&
            const DeepCollectionEquality().equals(other.location, location) &&
            const DeepCollectionEquality()
                .equals(other.coverageAreas, coverageAreas) &&
            const DeepCollectionEquality()
                .equals(other.attributes, attributes) &&
            const DeepCollectionEquality().equals(other.warnings, warnings) &&
            const DeepCollectionEquality()
                .equals(other.listingSource, listingSource) &&
            const DeepCollectionEquality()
                .equals(other.variations, variations) &&
            const DeepCollectionEquality().equals(other.status, status) &&
            const DeepCollectionEquality().equals(other.subStatus, subStatus) &&
            const DeepCollectionEquality().equals(other.tags, tags) &&
            const DeepCollectionEquality().equals(other.warranty, warranty) &&
            const DeepCollectionEquality()
                .equals(other.catalogProductId, catalogProductId) &&
            const DeepCollectionEquality().equals(other.domainId, domainId) &&
            const DeepCollectionEquality()
                .equals(other.parentItemId, parentItemId) &&
            const DeepCollectionEquality()
                .equals(other.differentialPricing, differentialPricing) &&
            const DeepCollectionEquality().equals(other.dealIds, dealIds) &&
            const DeepCollectionEquality()
                .equals(other.automaticRelist, automaticRelist) &&
            const DeepCollectionEquality()
                .equals(other.dateCreated, dateCreated) &&
            const DeepCollectionEquality()
                .equals(other.lastUpdated, lastUpdated) &&
            const DeepCollectionEquality().equals(other.health, health) &&
            const DeepCollectionEquality()
                .equals(other.catalogListing, catalogListing) &&
            const DeepCollectionEquality().equals(other.channels, channels));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(id),
        const DeepCollectionEquality().hash(siteId),
        const DeepCollectionEquality().hash(title),
        const DeepCollectionEquality().hash(subtitle),
        const DeepCollectionEquality().hash(sellerId),
        const DeepCollectionEquality().hash(categoryId),
        const DeepCollectionEquality().hash(officialStoreId),
        const DeepCollectionEquality().hash(price),
        const DeepCollectionEquality().hash(basePrice),
        const DeepCollectionEquality().hash(originalPrice),
        const DeepCollectionEquality().hash(currencyId),
        const DeepCollectionEquality().hash(initial_quantity),
        const DeepCollectionEquality().hash(available_quantity),
        const DeepCollectionEquality().hash(sold_quantity),
        const DeepCollectionEquality().hash(saleTerms),
        const DeepCollectionEquality().hash(buyingMode),
        const DeepCollectionEquality().hash(listingTypeId),
        const DeepCollectionEquality().hash(startTime),
        const DeepCollectionEquality().hash(stopTime),
        const DeepCollectionEquality().hash(condition),
        const DeepCollectionEquality().hash(permalink),
        const DeepCollectionEquality().hash(thumbnailId),
        const DeepCollectionEquality().hash(thumbnail),
        const DeepCollectionEquality().hash(secureThumbnail),
        const DeepCollectionEquality().hash(pictures),
        const DeepCollectionEquality().hash(videoId),
        const DeepCollectionEquality().hash(descriptions),
        const DeepCollectionEquality().hash(acceptsMercadopago),
        const DeepCollectionEquality().hash(nonMercadoPagoPaymentMethods),
        const DeepCollectionEquality().hash(shipping),
        const DeepCollectionEquality().hash(doubleernationalDeliveryMode),
        const DeepCollectionEquality().hash(sellerAddress),
        const DeepCollectionEquality().hash(sellerContact),
        const DeepCollectionEquality().hash(location),
        const DeepCollectionEquality().hash(coverageAreas),
        const DeepCollectionEquality().hash(attributes),
        const DeepCollectionEquality().hash(warnings),
        const DeepCollectionEquality().hash(listingSource),
        const DeepCollectionEquality().hash(variations),
        const DeepCollectionEquality().hash(status),
        const DeepCollectionEquality().hash(subStatus),
        const DeepCollectionEquality().hash(tags),
        const DeepCollectionEquality().hash(warranty),
        const DeepCollectionEquality().hash(catalogProductId),
        const DeepCollectionEquality().hash(domainId),
        const DeepCollectionEquality().hash(parentItemId),
        const DeepCollectionEquality().hash(differentialPricing),
        const DeepCollectionEquality().hash(dealIds),
        const DeepCollectionEquality().hash(automaticRelist),
        const DeepCollectionEquality().hash(dateCreated),
        const DeepCollectionEquality().hash(lastUpdated),
        const DeepCollectionEquality().hash(health),
        const DeepCollectionEquality().hash(catalogListing),
        const DeepCollectionEquality().hash(channels)
      ]);

  @JsonKey(ignore: true)
  @override
  _$ProductCopyWith<_Product> get copyWith =>
      __$ProductCopyWithImpl<_Product>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProductToJson(this);
  }
}

abstract class _Product implements Product {
  const factory _Product(
      String? id,
      dynamic siteId,
      String? title,
      dynamic subtitle,
      double? sellerId,
      dynamic categoryId,
      dynamic officialStoreId,
      double? price,
      double? basePrice,
      dynamic originalPrice,
      dynamic currencyId,
      int? initial_quantity,
      int? available_quantity,
      int? sold_quantity,
      List<dynamic>? saleTerms,
      dynamic buyingMode,
      dynamic listingTypeId,
      dynamic startTime,
      dynamic stopTime,
      dynamic condition,
      dynamic permalink,
      dynamic thumbnailId,
      dynamic thumbnail,
      dynamic secureThumbnail,
      List<Picture>? pictures,
      dynamic videoId,
      List<dynamic>? descriptions,
      dynamic acceptsMercadopago,
      List<dynamic>? nonMercadoPagoPaymentMethods,
      dynamic shipping,
      dynamic doubleernationalDeliveryMode,
      dynamic sellerAddress,
      dynamic sellerContact,
      dynamic location,
      List<dynamic>? coverageAreas,
      List<dynamic>? attributes,
      List<dynamic>? warnings,
      dynamic listingSource,
      List<dynamic>? variations,
      dynamic status,
      List<dynamic>? subStatus,
      List<String>? tags,
      String? warranty,
      dynamic catalogProductId,
      dynamic domainId,
      dynamic parentItemId,
      dynamic differentialPricing,
      List<dynamic>? dealIds,
      bool? automaticRelist,
      dynamic dateCreated,
      dynamic lastUpdated,
      dynamic health,
      dynamic catalogListing,
      List<dynamic>? channels) = _$_Product;

  factory _Product.fromJson(Map<String, dynamic> json) = _$_Product.fromJson;

  @override
  String? get id;
  @override
  dynamic get siteId;
  @override
  String? get title;
  @override
  dynamic get subtitle;
  @override
  double? get sellerId;
  @override
  dynamic get categoryId;
  @override
  dynamic get officialStoreId;
  @override
  double? get price;
  @override
  double? get basePrice;
  @override
  dynamic get originalPrice;
  @override
  dynamic get currencyId;
  @override
  int? get initial_quantity;
  @override
  int? get available_quantity;
  @override
  int? get sold_quantity;
  @override
  List<dynamic>? get saleTerms;
  @override
  dynamic get buyingMode;
  @override
  dynamic get listingTypeId;
  @override
  dynamic get startTime;
  @override
  dynamic get stopTime;
  @override
  dynamic get condition;
  @override
  dynamic get permalink;
  @override
  dynamic get thumbnailId;
  @override
  dynamic get thumbnail;
  @override
  dynamic get secureThumbnail;
  @override
  List<Picture>? get pictures;
  @override
  dynamic get videoId;
  @override
  List<dynamic>? get descriptions;
  @override
  dynamic get acceptsMercadopago;
  @override
  List<dynamic>? get nonMercadoPagoPaymentMethods;
  @override
  dynamic get shipping;
  @override
  dynamic get doubleernationalDeliveryMode;
  @override
  dynamic get sellerAddress;
  @override
  dynamic get sellerContact;
  @override
  dynamic get location;
  @override
  List<dynamic>? get coverageAreas;
  @override
  List<dynamic>? get attributes;
  @override
  List<dynamic>? get warnings;
  @override
  dynamic get listingSource;
  @override
  List<dynamic>? get variations;
  @override
  dynamic get status;
  @override
  List<dynamic>? get subStatus;
  @override
  List<String>? get tags;
  @override
  String? get warranty;
  @override
  dynamic get catalogProductId;
  @override
  dynamic get domainId;
  @override
  dynamic get parentItemId;
  @override
  dynamic get differentialPricing;
  @override
  List<dynamic>? get dealIds;
  @override
  bool? get automaticRelist;
  @override
  dynamic get dateCreated;
  @override
  dynamic get lastUpdated;
  @override
  dynamic get health;
  @override
  dynamic get catalogListing;
  @override
  List<dynamic>? get channels;
  @override
  @JsonKey(ignore: true)
  _$ProductCopyWith<_Product> get copyWith =>
      throw _privateConstructorUsedError;
}
