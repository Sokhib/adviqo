import 'package:freezed_annotation/freezed_annotation.dart';

part 'presentation.freezed.dart';
part 'presentation.g.dart';

@freezed
class Presentation with _$Presentation {
  const factory Presentation(String? display_currency) = _Presentation;
  factory Presentation.fromJson(Map<String, dynamic> json) => _$PresentationFromJson(json);
}
