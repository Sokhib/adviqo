// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'presentation.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Presentation _$PresentationFromJson(Map<String, dynamic> json) {
  return _Presentation.fromJson(json);
}

/// @nodoc
class _$PresentationTearOff {
  const _$PresentationTearOff();

  _Presentation call(String? display_currency) {
    return _Presentation(
      display_currency,
    );
  }

  Presentation fromJson(Map<String, Object?> json) {
    return Presentation.fromJson(json);
  }
}

/// @nodoc
const $Presentation = _$PresentationTearOff();

/// @nodoc
mixin _$Presentation {
  String? get display_currency => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PresentationCopyWith<Presentation> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PresentationCopyWith<$Res> {
  factory $PresentationCopyWith(
          Presentation value, $Res Function(Presentation) then) =
      _$PresentationCopyWithImpl<$Res>;
  $Res call({String? display_currency});
}

/// @nodoc
class _$PresentationCopyWithImpl<$Res> implements $PresentationCopyWith<$Res> {
  _$PresentationCopyWithImpl(this._value, this._then);

  final Presentation _value;
  // ignore: unused_field
  final $Res Function(Presentation) _then;

  @override
  $Res call({
    Object? display_currency = freezed,
  }) {
    return _then(_value.copyWith(
      display_currency: display_currency == freezed
          ? _value.display_currency
          : display_currency // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$PresentationCopyWith<$Res>
    implements $PresentationCopyWith<$Res> {
  factory _$PresentationCopyWith(
          _Presentation value, $Res Function(_Presentation) then) =
      __$PresentationCopyWithImpl<$Res>;
  @override
  $Res call({String? display_currency});
}

/// @nodoc
class __$PresentationCopyWithImpl<$Res> extends _$PresentationCopyWithImpl<$Res>
    implements _$PresentationCopyWith<$Res> {
  __$PresentationCopyWithImpl(
      _Presentation _value, $Res Function(_Presentation) _then)
      : super(_value, (v) => _then(v as _Presentation));

  @override
  _Presentation get _value => super._value as _Presentation;

  @override
  $Res call({
    Object? display_currency = freezed,
  }) {
    return _then(_Presentation(
      display_currency == freezed
          ? _value.display_currency
          : display_currency // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Presentation implements _Presentation {
  const _$_Presentation(this.display_currency);

  factory _$_Presentation.fromJson(Map<String, dynamic> json) =>
      _$$_PresentationFromJson(json);

  @override
  final String? display_currency;

  @override
  String toString() {
    return 'Presentation(display_currency: $display_currency)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Presentation &&
            const DeepCollectionEquality()
                .equals(other.display_currency, display_currency));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(display_currency));

  @JsonKey(ignore: true)
  @override
  _$PresentationCopyWith<_Presentation> get copyWith =>
      __$PresentationCopyWithImpl<_Presentation>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PresentationToJson(this);
  }
}

abstract class _Presentation implements Presentation {
  const factory _Presentation(String? display_currency) = _$_Presentation;

  factory _Presentation.fromJson(Map<String, dynamic> json) =
      _$_Presentation.fromJson;

  @override
  String? get display_currency;
  @override
  @JsonKey(ignore: true)
  _$PresentationCopyWith<_Presentation> get copyWith =>
      throw _privateConstructorUsedError;
}
