import 'package:adviqo/data/models/search_result/presentation/presentation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'prices.freezed.dart';
part 'prices.g.dart';

@freezed
class Prices with _$Prices {
  const factory Prices(
    String? id,
    List<dynamic> prices,
    Presentation presentation,
    dynamic paymentMethodPrices,
    dynamic referencePrices,
    dynamic purchaseDiscounts,
  ) = _Prices;

  factory Prices.fromJson(Map<String, dynamic> json) => _$PricesFromJson(json);
}
