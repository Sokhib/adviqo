// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'prices.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Prices _$PricesFromJson(Map<String, dynamic> json) {
  return _Prices.fromJson(json);
}

/// @nodoc
class _$PricesTearOff {
  const _$PricesTearOff();

  _Prices call(
      String? id,
      List<dynamic> prices,
      Presentation presentation,
      dynamic paymentMethodPrices,
      dynamic referencePrices,
      dynamic purchaseDiscounts) {
    return _Prices(
      id,
      prices,
      presentation,
      paymentMethodPrices,
      referencePrices,
      purchaseDiscounts,
    );
  }

  Prices fromJson(Map<String, Object?> json) {
    return Prices.fromJson(json);
  }
}

/// @nodoc
const $Prices = _$PricesTearOff();

/// @nodoc
mixin _$Prices {
  String? get id => throw _privateConstructorUsedError;
  List<dynamic> get prices => throw _privateConstructorUsedError;
  Presentation get presentation => throw _privateConstructorUsedError;
  dynamic get paymentMethodPrices => throw _privateConstructorUsedError;
  dynamic get referencePrices => throw _privateConstructorUsedError;
  dynamic get purchaseDiscounts => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PricesCopyWith<Prices> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PricesCopyWith<$Res> {
  factory $PricesCopyWith(Prices value, $Res Function(Prices) then) =
      _$PricesCopyWithImpl<$Res>;
  $Res call(
      {String? id,
      List<dynamic> prices,
      Presentation presentation,
      dynamic paymentMethodPrices,
      dynamic referencePrices,
      dynamic purchaseDiscounts});

  $PresentationCopyWith<$Res> get presentation;
}

/// @nodoc
class _$PricesCopyWithImpl<$Res> implements $PricesCopyWith<$Res> {
  _$PricesCopyWithImpl(this._value, this._then);

  final Prices _value;
  // ignore: unused_field
  final $Res Function(Prices) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? prices = freezed,
    Object? presentation = freezed,
    Object? paymentMethodPrices = freezed,
    Object? referencePrices = freezed,
    Object? purchaseDiscounts = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      prices: prices == freezed
          ? _value.prices
          : prices // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      presentation: presentation == freezed
          ? _value.presentation
          : presentation // ignore: cast_nullable_to_non_nullable
              as Presentation,
      paymentMethodPrices: paymentMethodPrices == freezed
          ? _value.paymentMethodPrices
          : paymentMethodPrices // ignore: cast_nullable_to_non_nullable
              as dynamic,
      referencePrices: referencePrices == freezed
          ? _value.referencePrices
          : referencePrices // ignore: cast_nullable_to_non_nullable
              as dynamic,
      purchaseDiscounts: purchaseDiscounts == freezed
          ? _value.purchaseDiscounts
          : purchaseDiscounts // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }

  @override
  $PresentationCopyWith<$Res> get presentation {
    return $PresentationCopyWith<$Res>(_value.presentation, (value) {
      return _then(_value.copyWith(presentation: value));
    });
  }
}

/// @nodoc
abstract class _$PricesCopyWith<$Res> implements $PricesCopyWith<$Res> {
  factory _$PricesCopyWith(_Prices value, $Res Function(_Prices) then) =
      __$PricesCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? id,
      List<dynamic> prices,
      Presentation presentation,
      dynamic paymentMethodPrices,
      dynamic referencePrices,
      dynamic purchaseDiscounts});

  @override
  $PresentationCopyWith<$Res> get presentation;
}

/// @nodoc
class __$PricesCopyWithImpl<$Res> extends _$PricesCopyWithImpl<$Res>
    implements _$PricesCopyWith<$Res> {
  __$PricesCopyWithImpl(_Prices _value, $Res Function(_Prices) _then)
      : super(_value, (v) => _then(v as _Prices));

  @override
  _Prices get _value => super._value as _Prices;

  @override
  $Res call({
    Object? id = freezed,
    Object? prices = freezed,
    Object? presentation = freezed,
    Object? paymentMethodPrices = freezed,
    Object? referencePrices = freezed,
    Object? purchaseDiscounts = freezed,
  }) {
    return _then(_Prices(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      prices == freezed
          ? _value.prices
          : prices // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      presentation == freezed
          ? _value.presentation
          : presentation // ignore: cast_nullable_to_non_nullable
              as Presentation,
      paymentMethodPrices == freezed
          ? _value.paymentMethodPrices
          : paymentMethodPrices // ignore: cast_nullable_to_non_nullable
              as dynamic,
      referencePrices == freezed
          ? _value.referencePrices
          : referencePrices // ignore: cast_nullable_to_non_nullable
              as dynamic,
      purchaseDiscounts == freezed
          ? _value.purchaseDiscounts
          : purchaseDiscounts // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Prices implements _Prices {
  const _$_Prices(this.id, this.prices, this.presentation,
      this.paymentMethodPrices, this.referencePrices, this.purchaseDiscounts);

  factory _$_Prices.fromJson(Map<String, dynamic> json) =>
      _$$_PricesFromJson(json);

  @override
  final String? id;
  @override
  final List<dynamic> prices;
  @override
  final Presentation presentation;
  @override
  final dynamic paymentMethodPrices;
  @override
  final dynamic referencePrices;
  @override
  final dynamic purchaseDiscounts;

  @override
  String toString() {
    return 'Prices(id: $id, prices: $prices, presentation: $presentation, paymentMethodPrices: $paymentMethodPrices, referencePrices: $referencePrices, purchaseDiscounts: $purchaseDiscounts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Prices &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.prices, prices) &&
            const DeepCollectionEquality()
                .equals(other.presentation, presentation) &&
            const DeepCollectionEquality()
                .equals(other.paymentMethodPrices, paymentMethodPrices) &&
            const DeepCollectionEquality()
                .equals(other.referencePrices, referencePrices) &&
            const DeepCollectionEquality()
                .equals(other.purchaseDiscounts, purchaseDiscounts));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(prices),
      const DeepCollectionEquality().hash(presentation),
      const DeepCollectionEquality().hash(paymentMethodPrices),
      const DeepCollectionEquality().hash(referencePrices),
      const DeepCollectionEquality().hash(purchaseDiscounts));

  @JsonKey(ignore: true)
  @override
  _$PricesCopyWith<_Prices> get copyWith =>
      __$PricesCopyWithImpl<_Prices>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PricesToJson(this);
  }
}

abstract class _Prices implements Prices {
  const factory _Prices(
      String? id,
      List<dynamic> prices,
      Presentation presentation,
      dynamic paymentMethodPrices,
      dynamic referencePrices,
      dynamic purchaseDiscounts) = _$_Prices;

  factory _Prices.fromJson(Map<String, dynamic> json) = _$_Prices.fromJson;

  @override
  String? get id;
  @override
  List<dynamic> get prices;
  @override
  Presentation get presentation;
  @override
  dynamic get paymentMethodPrices;
  @override
  dynamic get referencePrices;
  @override
  dynamic get purchaseDiscounts;
  @override
  @JsonKey(ignore: true)
  _$PricesCopyWith<_Prices> get copyWith => throw _privateConstructorUsedError;
}
