// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prices.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Prices _$$_PricesFromJson(Map<String, dynamic> json) => _$_Prices(
      json['id'] as String?,
      json['prices'] as List<dynamic>,
      Presentation.fromJson(json['presentation'] as Map<String, dynamic>),
      json['paymentMethodPrices'],
      json['referencePrices'],
      json['purchaseDiscounts'],
    );

Map<String, dynamic> _$$_PricesToJson(_$_Prices instance) => <String, dynamic>{
      'id': instance.id,
      'prices': instance.prices,
      'presentation': instance.presentation,
      'paymentMethodPrices': instance.paymentMethodPrices,
      'referencePrices': instance.referencePrices,
      'purchaseDiscounts': instance.purchaseDiscounts,
    };
