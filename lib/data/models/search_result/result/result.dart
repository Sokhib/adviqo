import 'package:adviqo/data/models/search_result/prices/prices.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'result.freezed.dart';
part 'result.g.dart';

@freezed
class Result with _$Result {
  const factory Result({
    String? id,
    dynamic siteId,
    String? title,
    dynamic seller,
    double? price,
    Prices? prices,
    dynamic salePrice,
    dynamic currencyId,
    int? availableQuantity,
    int? soldQuantity,
    dynamic buyingMode,
    dynamic listingTypeId,
    dynamic stopTime,
    dynamic condition,
    dynamic permalink,
    dynamic thumbnail,
    dynamic thumbnailId,
    bool? acceptsMercadopago,
    dynamic installments,
    dynamic address,
    dynamic shipping,
    dynamic sellerAddress,
    dynamic attributes,
    int? originalPrice,
    dynamic categoryId,
    int? officialStoreId,
    dynamic domainId,
    dynamic catalogProductId,
    dynamic tags,
    dynamic orderBackend,
    dynamic useThumbnailId,
    dynamic offerScore,
    dynamic offerShare,
    dynamic matchScore,
    dynamic winnerItemId,
    dynamic melicoin,
    dynamic sellerContact,
    dynamic location,
    bool? catalogListing,
  }) = _Result;

  factory Result.fromJson(Map<String, dynamic> json) => _$ResultFromJson(json);
}
