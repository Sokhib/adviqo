// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'result.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Result _$ResultFromJson(Map<String, dynamic> json) {
  return _Result.fromJson(json);
}

/// @nodoc
class _$ResultTearOff {
  const _$ResultTearOff();

  _Result call(
      {String? id,
      dynamic siteId,
      String? title,
      dynamic seller,
      double? price,
      Prices? prices,
      dynamic salePrice,
      dynamic currencyId,
      int? availableQuantity,
      int? soldQuantity,
      dynamic buyingMode,
      dynamic listingTypeId,
      dynamic stopTime,
      dynamic condition,
      dynamic permalink,
      dynamic thumbnail,
      dynamic thumbnailId,
      bool? acceptsMercadopago,
      dynamic installments,
      dynamic address,
      dynamic shipping,
      dynamic sellerAddress,
      dynamic attributes,
      int? originalPrice,
      dynamic categoryId,
      int? officialStoreId,
      dynamic domainId,
      dynamic catalogProductId,
      dynamic tags,
      dynamic orderBackend,
      dynamic useThumbnailId,
      dynamic offerScore,
      dynamic offerShare,
      dynamic matchScore,
      dynamic winnerItemId,
      dynamic melicoin,
      dynamic sellerContact,
      dynamic location,
      bool? catalogListing}) {
    return _Result(
      id: id,
      siteId: siteId,
      title: title,
      seller: seller,
      price: price,
      prices: prices,
      salePrice: salePrice,
      currencyId: currencyId,
      availableQuantity: availableQuantity,
      soldQuantity: soldQuantity,
      buyingMode: buyingMode,
      listingTypeId: listingTypeId,
      stopTime: stopTime,
      condition: condition,
      permalink: permalink,
      thumbnail: thumbnail,
      thumbnailId: thumbnailId,
      acceptsMercadopago: acceptsMercadopago,
      installments: installments,
      address: address,
      shipping: shipping,
      sellerAddress: sellerAddress,
      attributes: attributes,
      originalPrice: originalPrice,
      categoryId: categoryId,
      officialStoreId: officialStoreId,
      domainId: domainId,
      catalogProductId: catalogProductId,
      tags: tags,
      orderBackend: orderBackend,
      useThumbnailId: useThumbnailId,
      offerScore: offerScore,
      offerShare: offerShare,
      matchScore: matchScore,
      winnerItemId: winnerItemId,
      melicoin: melicoin,
      sellerContact: sellerContact,
      location: location,
      catalogListing: catalogListing,
    );
  }

  Result fromJson(Map<String, Object?> json) {
    return Result.fromJson(json);
  }
}

/// @nodoc
const $Result = _$ResultTearOff();

/// @nodoc
mixin _$Result {
  String? get id => throw _privateConstructorUsedError;
  dynamic get siteId => throw _privateConstructorUsedError;
  String? get title => throw _privateConstructorUsedError;
  dynamic get seller => throw _privateConstructorUsedError;
  double? get price => throw _privateConstructorUsedError;
  Prices? get prices => throw _privateConstructorUsedError;
  dynamic get salePrice => throw _privateConstructorUsedError;
  dynamic get currencyId => throw _privateConstructorUsedError;
  int? get availableQuantity => throw _privateConstructorUsedError;
  int? get soldQuantity => throw _privateConstructorUsedError;
  dynamic get buyingMode => throw _privateConstructorUsedError;
  dynamic get listingTypeId => throw _privateConstructorUsedError;
  dynamic get stopTime => throw _privateConstructorUsedError;
  dynamic get condition => throw _privateConstructorUsedError;
  dynamic get permalink => throw _privateConstructorUsedError;
  dynamic get thumbnail => throw _privateConstructorUsedError;
  dynamic get thumbnailId => throw _privateConstructorUsedError;
  bool? get acceptsMercadopago => throw _privateConstructorUsedError;
  dynamic get installments => throw _privateConstructorUsedError;
  dynamic get address => throw _privateConstructorUsedError;
  dynamic get shipping => throw _privateConstructorUsedError;
  dynamic get sellerAddress => throw _privateConstructorUsedError;
  dynamic get attributes => throw _privateConstructorUsedError;
  int? get originalPrice => throw _privateConstructorUsedError;
  dynamic get categoryId => throw _privateConstructorUsedError;
  int? get officialStoreId => throw _privateConstructorUsedError;
  dynamic get domainId => throw _privateConstructorUsedError;
  dynamic get catalogProductId => throw _privateConstructorUsedError;
  dynamic get tags => throw _privateConstructorUsedError;
  dynamic get orderBackend => throw _privateConstructorUsedError;
  dynamic get useThumbnailId => throw _privateConstructorUsedError;
  dynamic get offerScore => throw _privateConstructorUsedError;
  dynamic get offerShare => throw _privateConstructorUsedError;
  dynamic get matchScore => throw _privateConstructorUsedError;
  dynamic get winnerItemId => throw _privateConstructorUsedError;
  dynamic get melicoin => throw _privateConstructorUsedError;
  dynamic get sellerContact => throw _privateConstructorUsedError;
  dynamic get location => throw _privateConstructorUsedError;
  bool? get catalogListing => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ResultCopyWith<Result> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResultCopyWith<$Res> {
  factory $ResultCopyWith(Result value, $Res Function(Result) then) =
      _$ResultCopyWithImpl<$Res>;
  $Res call(
      {String? id,
      dynamic siteId,
      String? title,
      dynamic seller,
      double? price,
      Prices? prices,
      dynamic salePrice,
      dynamic currencyId,
      int? availableQuantity,
      int? soldQuantity,
      dynamic buyingMode,
      dynamic listingTypeId,
      dynamic stopTime,
      dynamic condition,
      dynamic permalink,
      dynamic thumbnail,
      dynamic thumbnailId,
      bool? acceptsMercadopago,
      dynamic installments,
      dynamic address,
      dynamic shipping,
      dynamic sellerAddress,
      dynamic attributes,
      int? originalPrice,
      dynamic categoryId,
      int? officialStoreId,
      dynamic domainId,
      dynamic catalogProductId,
      dynamic tags,
      dynamic orderBackend,
      dynamic useThumbnailId,
      dynamic offerScore,
      dynamic offerShare,
      dynamic matchScore,
      dynamic winnerItemId,
      dynamic melicoin,
      dynamic sellerContact,
      dynamic location,
      bool? catalogListing});

  $PricesCopyWith<$Res>? get prices;
}

/// @nodoc
class _$ResultCopyWithImpl<$Res> implements $ResultCopyWith<$Res> {
  _$ResultCopyWithImpl(this._value, this._then);

  final Result _value;
  // ignore: unused_field
  final $Res Function(Result) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? siteId = freezed,
    Object? title = freezed,
    Object? seller = freezed,
    Object? price = freezed,
    Object? prices = freezed,
    Object? salePrice = freezed,
    Object? currencyId = freezed,
    Object? availableQuantity = freezed,
    Object? soldQuantity = freezed,
    Object? buyingMode = freezed,
    Object? listingTypeId = freezed,
    Object? stopTime = freezed,
    Object? condition = freezed,
    Object? permalink = freezed,
    Object? thumbnail = freezed,
    Object? thumbnailId = freezed,
    Object? acceptsMercadopago = freezed,
    Object? installments = freezed,
    Object? address = freezed,
    Object? shipping = freezed,
    Object? sellerAddress = freezed,
    Object? attributes = freezed,
    Object? originalPrice = freezed,
    Object? categoryId = freezed,
    Object? officialStoreId = freezed,
    Object? domainId = freezed,
    Object? catalogProductId = freezed,
    Object? tags = freezed,
    Object? orderBackend = freezed,
    Object? useThumbnailId = freezed,
    Object? offerScore = freezed,
    Object? offerShare = freezed,
    Object? matchScore = freezed,
    Object? winnerItemId = freezed,
    Object? melicoin = freezed,
    Object? sellerContact = freezed,
    Object? location = freezed,
    Object? catalogListing = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      siteId: siteId == freezed
          ? _value.siteId
          : siteId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      seller: seller == freezed
          ? _value.seller
          : seller // ignore: cast_nullable_to_non_nullable
              as dynamic,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double?,
      prices: prices == freezed
          ? _value.prices
          : prices // ignore: cast_nullable_to_non_nullable
              as Prices?,
      salePrice: salePrice == freezed
          ? _value.salePrice
          : salePrice // ignore: cast_nullable_to_non_nullable
              as dynamic,
      currencyId: currencyId == freezed
          ? _value.currencyId
          : currencyId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      availableQuantity: availableQuantity == freezed
          ? _value.availableQuantity
          : availableQuantity // ignore: cast_nullable_to_non_nullable
              as int?,
      soldQuantity: soldQuantity == freezed
          ? _value.soldQuantity
          : soldQuantity // ignore: cast_nullable_to_non_nullable
              as int?,
      buyingMode: buyingMode == freezed
          ? _value.buyingMode
          : buyingMode // ignore: cast_nullable_to_non_nullable
              as dynamic,
      listingTypeId: listingTypeId == freezed
          ? _value.listingTypeId
          : listingTypeId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      stopTime: stopTime == freezed
          ? _value.stopTime
          : stopTime // ignore: cast_nullable_to_non_nullable
              as dynamic,
      condition: condition == freezed
          ? _value.condition
          : condition // ignore: cast_nullable_to_non_nullable
              as dynamic,
      permalink: permalink == freezed
          ? _value.permalink
          : permalink // ignore: cast_nullable_to_non_nullable
              as dynamic,
      thumbnail: thumbnail == freezed
          ? _value.thumbnail
          : thumbnail // ignore: cast_nullable_to_non_nullable
              as dynamic,
      thumbnailId: thumbnailId == freezed
          ? _value.thumbnailId
          : thumbnailId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      acceptsMercadopago: acceptsMercadopago == freezed
          ? _value.acceptsMercadopago
          : acceptsMercadopago // ignore: cast_nullable_to_non_nullable
              as bool?,
      installments: installments == freezed
          ? _value.installments
          : installments // ignore: cast_nullable_to_non_nullable
              as dynamic,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as dynamic,
      shipping: shipping == freezed
          ? _value.shipping
          : shipping // ignore: cast_nullable_to_non_nullable
              as dynamic,
      sellerAddress: sellerAddress == freezed
          ? _value.sellerAddress
          : sellerAddress // ignore: cast_nullable_to_non_nullable
              as dynamic,
      attributes: attributes == freezed
          ? _value.attributes
          : attributes // ignore: cast_nullable_to_non_nullable
              as dynamic,
      originalPrice: originalPrice == freezed
          ? _value.originalPrice
          : originalPrice // ignore: cast_nullable_to_non_nullable
              as int?,
      categoryId: categoryId == freezed
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      officialStoreId: officialStoreId == freezed
          ? _value.officialStoreId
          : officialStoreId // ignore: cast_nullable_to_non_nullable
              as int?,
      domainId: domainId == freezed
          ? _value.domainId
          : domainId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      catalogProductId: catalogProductId == freezed
          ? _value.catalogProductId
          : catalogProductId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      tags: tags == freezed
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as dynamic,
      orderBackend: orderBackend == freezed
          ? _value.orderBackend
          : orderBackend // ignore: cast_nullable_to_non_nullable
              as dynamic,
      useThumbnailId: useThumbnailId == freezed
          ? _value.useThumbnailId
          : useThumbnailId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      offerScore: offerScore == freezed
          ? _value.offerScore
          : offerScore // ignore: cast_nullable_to_non_nullable
              as dynamic,
      offerShare: offerShare == freezed
          ? _value.offerShare
          : offerShare // ignore: cast_nullable_to_non_nullable
              as dynamic,
      matchScore: matchScore == freezed
          ? _value.matchScore
          : matchScore // ignore: cast_nullable_to_non_nullable
              as dynamic,
      winnerItemId: winnerItemId == freezed
          ? _value.winnerItemId
          : winnerItemId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      melicoin: melicoin == freezed
          ? _value.melicoin
          : melicoin // ignore: cast_nullable_to_non_nullable
              as dynamic,
      sellerContact: sellerContact == freezed
          ? _value.sellerContact
          : sellerContact // ignore: cast_nullable_to_non_nullable
              as dynamic,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as dynamic,
      catalogListing: catalogListing == freezed
          ? _value.catalogListing
          : catalogListing // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }

  @override
  $PricesCopyWith<$Res>? get prices {
    if (_value.prices == null) {
      return null;
    }

    return $PricesCopyWith<$Res>(_value.prices!, (value) {
      return _then(_value.copyWith(prices: value));
    });
  }
}

/// @nodoc
abstract class _$ResultCopyWith<$Res> implements $ResultCopyWith<$Res> {
  factory _$ResultCopyWith(_Result value, $Res Function(_Result) then) =
      __$ResultCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? id,
      dynamic siteId,
      String? title,
      dynamic seller,
      double? price,
      Prices? prices,
      dynamic salePrice,
      dynamic currencyId,
      int? availableQuantity,
      int? soldQuantity,
      dynamic buyingMode,
      dynamic listingTypeId,
      dynamic stopTime,
      dynamic condition,
      dynamic permalink,
      dynamic thumbnail,
      dynamic thumbnailId,
      bool? acceptsMercadopago,
      dynamic installments,
      dynamic address,
      dynamic shipping,
      dynamic sellerAddress,
      dynamic attributes,
      int? originalPrice,
      dynamic categoryId,
      int? officialStoreId,
      dynamic domainId,
      dynamic catalogProductId,
      dynamic tags,
      dynamic orderBackend,
      dynamic useThumbnailId,
      dynamic offerScore,
      dynamic offerShare,
      dynamic matchScore,
      dynamic winnerItemId,
      dynamic melicoin,
      dynamic sellerContact,
      dynamic location,
      bool? catalogListing});

  @override
  $PricesCopyWith<$Res>? get prices;
}

/// @nodoc
class __$ResultCopyWithImpl<$Res> extends _$ResultCopyWithImpl<$Res>
    implements _$ResultCopyWith<$Res> {
  __$ResultCopyWithImpl(_Result _value, $Res Function(_Result) _then)
      : super(_value, (v) => _then(v as _Result));

  @override
  _Result get _value => super._value as _Result;

  @override
  $Res call({
    Object? id = freezed,
    Object? siteId = freezed,
    Object? title = freezed,
    Object? seller = freezed,
    Object? price = freezed,
    Object? prices = freezed,
    Object? salePrice = freezed,
    Object? currencyId = freezed,
    Object? availableQuantity = freezed,
    Object? soldQuantity = freezed,
    Object? buyingMode = freezed,
    Object? listingTypeId = freezed,
    Object? stopTime = freezed,
    Object? condition = freezed,
    Object? permalink = freezed,
    Object? thumbnail = freezed,
    Object? thumbnailId = freezed,
    Object? acceptsMercadopago = freezed,
    Object? installments = freezed,
    Object? address = freezed,
    Object? shipping = freezed,
    Object? sellerAddress = freezed,
    Object? attributes = freezed,
    Object? originalPrice = freezed,
    Object? categoryId = freezed,
    Object? officialStoreId = freezed,
    Object? domainId = freezed,
    Object? catalogProductId = freezed,
    Object? tags = freezed,
    Object? orderBackend = freezed,
    Object? useThumbnailId = freezed,
    Object? offerScore = freezed,
    Object? offerShare = freezed,
    Object? matchScore = freezed,
    Object? winnerItemId = freezed,
    Object? melicoin = freezed,
    Object? sellerContact = freezed,
    Object? location = freezed,
    Object? catalogListing = freezed,
  }) {
    return _then(_Result(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      siteId: siteId == freezed
          ? _value.siteId
          : siteId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      seller: seller == freezed
          ? _value.seller
          : seller // ignore: cast_nullable_to_non_nullable
              as dynamic,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double?,
      prices: prices == freezed
          ? _value.prices
          : prices // ignore: cast_nullable_to_non_nullable
              as Prices?,
      salePrice: salePrice == freezed
          ? _value.salePrice
          : salePrice // ignore: cast_nullable_to_non_nullable
              as dynamic,
      currencyId: currencyId == freezed
          ? _value.currencyId
          : currencyId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      availableQuantity: availableQuantity == freezed
          ? _value.availableQuantity
          : availableQuantity // ignore: cast_nullable_to_non_nullable
              as int?,
      soldQuantity: soldQuantity == freezed
          ? _value.soldQuantity
          : soldQuantity // ignore: cast_nullable_to_non_nullable
              as int?,
      buyingMode: buyingMode == freezed
          ? _value.buyingMode
          : buyingMode // ignore: cast_nullable_to_non_nullable
              as dynamic,
      listingTypeId: listingTypeId == freezed
          ? _value.listingTypeId
          : listingTypeId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      stopTime: stopTime == freezed
          ? _value.stopTime
          : stopTime // ignore: cast_nullable_to_non_nullable
              as dynamic,
      condition: condition == freezed
          ? _value.condition
          : condition // ignore: cast_nullable_to_non_nullable
              as dynamic,
      permalink: permalink == freezed
          ? _value.permalink
          : permalink // ignore: cast_nullable_to_non_nullable
              as dynamic,
      thumbnail: thumbnail == freezed
          ? _value.thumbnail
          : thumbnail // ignore: cast_nullable_to_non_nullable
              as dynamic,
      thumbnailId: thumbnailId == freezed
          ? _value.thumbnailId
          : thumbnailId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      acceptsMercadopago: acceptsMercadopago == freezed
          ? _value.acceptsMercadopago
          : acceptsMercadopago // ignore: cast_nullable_to_non_nullable
              as bool?,
      installments: installments == freezed
          ? _value.installments
          : installments // ignore: cast_nullable_to_non_nullable
              as dynamic,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as dynamic,
      shipping: shipping == freezed
          ? _value.shipping
          : shipping // ignore: cast_nullable_to_non_nullable
              as dynamic,
      sellerAddress: sellerAddress == freezed
          ? _value.sellerAddress
          : sellerAddress // ignore: cast_nullable_to_non_nullable
              as dynamic,
      attributes: attributes == freezed
          ? _value.attributes
          : attributes // ignore: cast_nullable_to_non_nullable
              as dynamic,
      originalPrice: originalPrice == freezed
          ? _value.originalPrice
          : originalPrice // ignore: cast_nullable_to_non_nullable
              as int?,
      categoryId: categoryId == freezed
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      officialStoreId: officialStoreId == freezed
          ? _value.officialStoreId
          : officialStoreId // ignore: cast_nullable_to_non_nullable
              as int?,
      domainId: domainId == freezed
          ? _value.domainId
          : domainId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      catalogProductId: catalogProductId == freezed
          ? _value.catalogProductId
          : catalogProductId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      tags: tags == freezed
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as dynamic,
      orderBackend: orderBackend == freezed
          ? _value.orderBackend
          : orderBackend // ignore: cast_nullable_to_non_nullable
              as dynamic,
      useThumbnailId: useThumbnailId == freezed
          ? _value.useThumbnailId
          : useThumbnailId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      offerScore: offerScore == freezed
          ? _value.offerScore
          : offerScore // ignore: cast_nullable_to_non_nullable
              as dynamic,
      offerShare: offerShare == freezed
          ? _value.offerShare
          : offerShare // ignore: cast_nullable_to_non_nullable
              as dynamic,
      matchScore: matchScore == freezed
          ? _value.matchScore
          : matchScore // ignore: cast_nullable_to_non_nullable
              as dynamic,
      winnerItemId: winnerItemId == freezed
          ? _value.winnerItemId
          : winnerItemId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      melicoin: melicoin == freezed
          ? _value.melicoin
          : melicoin // ignore: cast_nullable_to_non_nullable
              as dynamic,
      sellerContact: sellerContact == freezed
          ? _value.sellerContact
          : sellerContact // ignore: cast_nullable_to_non_nullable
              as dynamic,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as dynamic,
      catalogListing: catalogListing == freezed
          ? _value.catalogListing
          : catalogListing // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Result implements _Result {
  const _$_Result(
      {this.id,
      this.siteId,
      this.title,
      this.seller,
      this.price,
      this.prices,
      this.salePrice,
      this.currencyId,
      this.availableQuantity,
      this.soldQuantity,
      this.buyingMode,
      this.listingTypeId,
      this.stopTime,
      this.condition,
      this.permalink,
      this.thumbnail,
      this.thumbnailId,
      this.acceptsMercadopago,
      this.installments,
      this.address,
      this.shipping,
      this.sellerAddress,
      this.attributes,
      this.originalPrice,
      this.categoryId,
      this.officialStoreId,
      this.domainId,
      this.catalogProductId,
      this.tags,
      this.orderBackend,
      this.useThumbnailId,
      this.offerScore,
      this.offerShare,
      this.matchScore,
      this.winnerItemId,
      this.melicoin,
      this.sellerContact,
      this.location,
      this.catalogListing});

  factory _$_Result.fromJson(Map<String, dynamic> json) =>
      _$$_ResultFromJson(json);

  @override
  final String? id;
  @override
  final dynamic siteId;
  @override
  final String? title;
  @override
  final dynamic seller;
  @override
  final double? price;
  @override
  final Prices? prices;
  @override
  final dynamic salePrice;
  @override
  final dynamic currencyId;
  @override
  final int? availableQuantity;
  @override
  final int? soldQuantity;
  @override
  final dynamic buyingMode;
  @override
  final dynamic listingTypeId;
  @override
  final dynamic stopTime;
  @override
  final dynamic condition;
  @override
  final dynamic permalink;
  @override
  final dynamic thumbnail;
  @override
  final dynamic thumbnailId;
  @override
  final bool? acceptsMercadopago;
  @override
  final dynamic installments;
  @override
  final dynamic address;
  @override
  final dynamic shipping;
  @override
  final dynamic sellerAddress;
  @override
  final dynamic attributes;
  @override
  final int? originalPrice;
  @override
  final dynamic categoryId;
  @override
  final int? officialStoreId;
  @override
  final dynamic domainId;
  @override
  final dynamic catalogProductId;
  @override
  final dynamic tags;
  @override
  final dynamic orderBackend;
  @override
  final dynamic useThumbnailId;
  @override
  final dynamic offerScore;
  @override
  final dynamic offerShare;
  @override
  final dynamic matchScore;
  @override
  final dynamic winnerItemId;
  @override
  final dynamic melicoin;
  @override
  final dynamic sellerContact;
  @override
  final dynamic location;
  @override
  final bool? catalogListing;

  @override
  String toString() {
    return 'Result(id: $id, siteId: $siteId, title: $title, seller: $seller, price: $price, prices: $prices, salePrice: $salePrice, currencyId: $currencyId, availableQuantity: $availableQuantity, soldQuantity: $soldQuantity, buyingMode: $buyingMode, listingTypeId: $listingTypeId, stopTime: $stopTime, condition: $condition, permalink: $permalink, thumbnail: $thumbnail, thumbnailId: $thumbnailId, acceptsMercadopago: $acceptsMercadopago, installments: $installments, address: $address, shipping: $shipping, sellerAddress: $sellerAddress, attributes: $attributes, originalPrice: $originalPrice, categoryId: $categoryId, officialStoreId: $officialStoreId, domainId: $domainId, catalogProductId: $catalogProductId, tags: $tags, orderBackend: $orderBackend, useThumbnailId: $useThumbnailId, offerScore: $offerScore, offerShare: $offerShare, matchScore: $matchScore, winnerItemId: $winnerItemId, melicoin: $melicoin, sellerContact: $sellerContact, location: $location, catalogListing: $catalogListing)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Result &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.siteId, siteId) &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.seller, seller) &&
            const DeepCollectionEquality().equals(other.price, price) &&
            const DeepCollectionEquality().equals(other.prices, prices) &&
            const DeepCollectionEquality().equals(other.salePrice, salePrice) &&
            const DeepCollectionEquality()
                .equals(other.currencyId, currencyId) &&
            const DeepCollectionEquality()
                .equals(other.availableQuantity, availableQuantity) &&
            const DeepCollectionEquality()
                .equals(other.soldQuantity, soldQuantity) &&
            const DeepCollectionEquality()
                .equals(other.buyingMode, buyingMode) &&
            const DeepCollectionEquality()
                .equals(other.listingTypeId, listingTypeId) &&
            const DeepCollectionEquality().equals(other.stopTime, stopTime) &&
            const DeepCollectionEquality().equals(other.condition, condition) &&
            const DeepCollectionEquality().equals(other.permalink, permalink) &&
            const DeepCollectionEquality().equals(other.thumbnail, thumbnail) &&
            const DeepCollectionEquality()
                .equals(other.thumbnailId, thumbnailId) &&
            const DeepCollectionEquality()
                .equals(other.acceptsMercadopago, acceptsMercadopago) &&
            const DeepCollectionEquality()
                .equals(other.installments, installments) &&
            const DeepCollectionEquality().equals(other.address, address) &&
            const DeepCollectionEquality().equals(other.shipping, shipping) &&
            const DeepCollectionEquality()
                .equals(other.sellerAddress, sellerAddress) &&
            const DeepCollectionEquality()
                .equals(other.attributes, attributes) &&
            const DeepCollectionEquality()
                .equals(other.originalPrice, originalPrice) &&
            const DeepCollectionEquality()
                .equals(other.categoryId, categoryId) &&
            const DeepCollectionEquality()
                .equals(other.officialStoreId, officialStoreId) &&
            const DeepCollectionEquality().equals(other.domainId, domainId) &&
            const DeepCollectionEquality()
                .equals(other.catalogProductId, catalogProductId) &&
            const DeepCollectionEquality().equals(other.tags, tags) &&
            const DeepCollectionEquality()
                .equals(other.orderBackend, orderBackend) &&
            const DeepCollectionEquality()
                .equals(other.useThumbnailId, useThumbnailId) &&
            const DeepCollectionEquality()
                .equals(other.offerScore, offerScore) &&
            const DeepCollectionEquality()
                .equals(other.offerShare, offerShare) &&
            const DeepCollectionEquality()
                .equals(other.matchScore, matchScore) &&
            const DeepCollectionEquality()
                .equals(other.winnerItemId, winnerItemId) &&
            const DeepCollectionEquality().equals(other.melicoin, melicoin) &&
            const DeepCollectionEquality()
                .equals(other.sellerContact, sellerContact) &&
            const DeepCollectionEquality().equals(other.location, location) &&
            const DeepCollectionEquality()
                .equals(other.catalogListing, catalogListing));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(id),
        const DeepCollectionEquality().hash(siteId),
        const DeepCollectionEquality().hash(title),
        const DeepCollectionEquality().hash(seller),
        const DeepCollectionEquality().hash(price),
        const DeepCollectionEquality().hash(prices),
        const DeepCollectionEquality().hash(salePrice),
        const DeepCollectionEquality().hash(currencyId),
        const DeepCollectionEquality().hash(availableQuantity),
        const DeepCollectionEquality().hash(soldQuantity),
        const DeepCollectionEquality().hash(buyingMode),
        const DeepCollectionEquality().hash(listingTypeId),
        const DeepCollectionEquality().hash(stopTime),
        const DeepCollectionEquality().hash(condition),
        const DeepCollectionEquality().hash(permalink),
        const DeepCollectionEquality().hash(thumbnail),
        const DeepCollectionEquality().hash(thumbnailId),
        const DeepCollectionEquality().hash(acceptsMercadopago),
        const DeepCollectionEquality().hash(installments),
        const DeepCollectionEquality().hash(address),
        const DeepCollectionEquality().hash(shipping),
        const DeepCollectionEquality().hash(sellerAddress),
        const DeepCollectionEquality().hash(attributes),
        const DeepCollectionEquality().hash(originalPrice),
        const DeepCollectionEquality().hash(categoryId),
        const DeepCollectionEquality().hash(officialStoreId),
        const DeepCollectionEquality().hash(domainId),
        const DeepCollectionEquality().hash(catalogProductId),
        const DeepCollectionEquality().hash(tags),
        const DeepCollectionEquality().hash(orderBackend),
        const DeepCollectionEquality().hash(useThumbnailId),
        const DeepCollectionEquality().hash(offerScore),
        const DeepCollectionEquality().hash(offerShare),
        const DeepCollectionEquality().hash(matchScore),
        const DeepCollectionEquality().hash(winnerItemId),
        const DeepCollectionEquality().hash(melicoin),
        const DeepCollectionEquality().hash(sellerContact),
        const DeepCollectionEquality().hash(location),
        const DeepCollectionEquality().hash(catalogListing)
      ]);

  @JsonKey(ignore: true)
  @override
  _$ResultCopyWith<_Result> get copyWith =>
      __$ResultCopyWithImpl<_Result>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ResultToJson(this);
  }
}

abstract class _Result implements Result {
  const factory _Result(
      {String? id,
      dynamic siteId,
      String? title,
      dynamic seller,
      double? price,
      Prices? prices,
      dynamic salePrice,
      dynamic currencyId,
      int? availableQuantity,
      int? soldQuantity,
      dynamic buyingMode,
      dynamic listingTypeId,
      dynamic stopTime,
      dynamic condition,
      dynamic permalink,
      dynamic thumbnail,
      dynamic thumbnailId,
      bool? acceptsMercadopago,
      dynamic installments,
      dynamic address,
      dynamic shipping,
      dynamic sellerAddress,
      dynamic attributes,
      int? originalPrice,
      dynamic categoryId,
      int? officialStoreId,
      dynamic domainId,
      dynamic catalogProductId,
      dynamic tags,
      dynamic orderBackend,
      dynamic useThumbnailId,
      dynamic offerScore,
      dynamic offerShare,
      dynamic matchScore,
      dynamic winnerItemId,
      dynamic melicoin,
      dynamic sellerContact,
      dynamic location,
      bool? catalogListing}) = _$_Result;

  factory _Result.fromJson(Map<String, dynamic> json) = _$_Result.fromJson;

  @override
  String? get id;
  @override
  dynamic get siteId;
  @override
  String? get title;
  @override
  dynamic get seller;
  @override
  double? get price;
  @override
  Prices? get prices;
  @override
  dynamic get salePrice;
  @override
  dynamic get currencyId;
  @override
  int? get availableQuantity;
  @override
  int? get soldQuantity;
  @override
  dynamic get buyingMode;
  @override
  dynamic get listingTypeId;
  @override
  dynamic get stopTime;
  @override
  dynamic get condition;
  @override
  dynamic get permalink;
  @override
  dynamic get thumbnail;
  @override
  dynamic get thumbnailId;
  @override
  bool? get acceptsMercadopago;
  @override
  dynamic get installments;
  @override
  dynamic get address;
  @override
  dynamic get shipping;
  @override
  dynamic get sellerAddress;
  @override
  dynamic get attributes;
  @override
  int? get originalPrice;
  @override
  dynamic get categoryId;
  @override
  int? get officialStoreId;
  @override
  dynamic get domainId;
  @override
  dynamic get catalogProductId;
  @override
  dynamic get tags;
  @override
  dynamic get orderBackend;
  @override
  dynamic get useThumbnailId;
  @override
  dynamic get offerScore;
  @override
  dynamic get offerShare;
  @override
  dynamic get matchScore;
  @override
  dynamic get winnerItemId;
  @override
  dynamic get melicoin;
  @override
  dynamic get sellerContact;
  @override
  dynamic get location;
  @override
  bool? get catalogListing;
  @override
  @JsonKey(ignore: true)
  _$ResultCopyWith<_Result> get copyWith => throw _privateConstructorUsedError;
}
