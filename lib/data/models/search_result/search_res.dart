import 'package:adviqo/data/models/search_result/result/result.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'search_res.freezed.dart';
part 'search_res.g.dart';

@freezed
class SearchResult with _$SearchResult {
  const factory SearchResult({
    String? siteId,
    dynamic countryDefaultTimeZone,
    String? query,
    dynamic paging,
    List<Result>? results,
    dynamic sort,
    dynamic availableSorts,
    dynamic filters,
    dynamic availableFilters,
  }) = _SearchResult;

  factory SearchResult.fromJson(Map<String, dynamic> json) => _$SearchResultFromJson(json);
}
