// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'search_res.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SearchResult _$SearchResultFromJson(Map<String, dynamic> json) {
  return _SearchResult.fromJson(json);
}

/// @nodoc
class _$SearchResultTearOff {
  const _$SearchResultTearOff();

  _SearchResult call(
      {String? siteId,
      dynamic countryDefaultTimeZone,
      String? query,
      dynamic paging,
      List<Result>? results,
      dynamic sort,
      dynamic availableSorts,
      dynamic filters,
      dynamic availableFilters}) {
    return _SearchResult(
      siteId: siteId,
      countryDefaultTimeZone: countryDefaultTimeZone,
      query: query,
      paging: paging,
      results: results,
      sort: sort,
      availableSorts: availableSorts,
      filters: filters,
      availableFilters: availableFilters,
    );
  }

  SearchResult fromJson(Map<String, Object?> json) {
    return SearchResult.fromJson(json);
  }
}

/// @nodoc
const $SearchResult = _$SearchResultTearOff();

/// @nodoc
mixin _$SearchResult {
  String? get siteId => throw _privateConstructorUsedError;
  dynamic get countryDefaultTimeZone => throw _privateConstructorUsedError;
  String? get query => throw _privateConstructorUsedError;
  dynamic get paging => throw _privateConstructorUsedError;
  List<Result>? get results => throw _privateConstructorUsedError;
  dynamic get sort => throw _privateConstructorUsedError;
  dynamic get availableSorts => throw _privateConstructorUsedError;
  dynamic get filters => throw _privateConstructorUsedError;
  dynamic get availableFilters => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SearchResultCopyWith<SearchResult> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SearchResultCopyWith<$Res> {
  factory $SearchResultCopyWith(
          SearchResult value, $Res Function(SearchResult) then) =
      _$SearchResultCopyWithImpl<$Res>;
  $Res call(
      {String? siteId,
      dynamic countryDefaultTimeZone,
      String? query,
      dynamic paging,
      List<Result>? results,
      dynamic sort,
      dynamic availableSorts,
      dynamic filters,
      dynamic availableFilters});
}

/// @nodoc
class _$SearchResultCopyWithImpl<$Res> implements $SearchResultCopyWith<$Res> {
  _$SearchResultCopyWithImpl(this._value, this._then);

  final SearchResult _value;
  // ignore: unused_field
  final $Res Function(SearchResult) _then;

  @override
  $Res call({
    Object? siteId = freezed,
    Object? countryDefaultTimeZone = freezed,
    Object? query = freezed,
    Object? paging = freezed,
    Object? results = freezed,
    Object? sort = freezed,
    Object? availableSorts = freezed,
    Object? filters = freezed,
    Object? availableFilters = freezed,
  }) {
    return _then(_value.copyWith(
      siteId: siteId == freezed
          ? _value.siteId
          : siteId // ignore: cast_nullable_to_non_nullable
              as String?,
      countryDefaultTimeZone: countryDefaultTimeZone == freezed
          ? _value.countryDefaultTimeZone
          : countryDefaultTimeZone // ignore: cast_nullable_to_non_nullable
              as dynamic,
      query: query == freezed
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String?,
      paging: paging == freezed
          ? _value.paging
          : paging // ignore: cast_nullable_to_non_nullable
              as dynamic,
      results: results == freezed
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Result>?,
      sort: sort == freezed
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as dynamic,
      availableSorts: availableSorts == freezed
          ? _value.availableSorts
          : availableSorts // ignore: cast_nullable_to_non_nullable
              as dynamic,
      filters: filters == freezed
          ? _value.filters
          : filters // ignore: cast_nullable_to_non_nullable
              as dynamic,
      availableFilters: availableFilters == freezed
          ? _value.availableFilters
          : availableFilters // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
abstract class _$SearchResultCopyWith<$Res>
    implements $SearchResultCopyWith<$Res> {
  factory _$SearchResultCopyWith(
          _SearchResult value, $Res Function(_SearchResult) then) =
      __$SearchResultCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? siteId,
      dynamic countryDefaultTimeZone,
      String? query,
      dynamic paging,
      List<Result>? results,
      dynamic sort,
      dynamic availableSorts,
      dynamic filters,
      dynamic availableFilters});
}

/// @nodoc
class __$SearchResultCopyWithImpl<$Res> extends _$SearchResultCopyWithImpl<$Res>
    implements _$SearchResultCopyWith<$Res> {
  __$SearchResultCopyWithImpl(
      _SearchResult _value, $Res Function(_SearchResult) _then)
      : super(_value, (v) => _then(v as _SearchResult));

  @override
  _SearchResult get _value => super._value as _SearchResult;

  @override
  $Res call({
    Object? siteId = freezed,
    Object? countryDefaultTimeZone = freezed,
    Object? query = freezed,
    Object? paging = freezed,
    Object? results = freezed,
    Object? sort = freezed,
    Object? availableSorts = freezed,
    Object? filters = freezed,
    Object? availableFilters = freezed,
  }) {
    return _then(_SearchResult(
      siteId: siteId == freezed
          ? _value.siteId
          : siteId // ignore: cast_nullable_to_non_nullable
              as String?,
      countryDefaultTimeZone: countryDefaultTimeZone == freezed
          ? _value.countryDefaultTimeZone
          : countryDefaultTimeZone // ignore: cast_nullable_to_non_nullable
              as dynamic,
      query: query == freezed
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String?,
      paging: paging == freezed
          ? _value.paging
          : paging // ignore: cast_nullable_to_non_nullable
              as dynamic,
      results: results == freezed
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Result>?,
      sort: sort == freezed
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as dynamic,
      availableSorts: availableSorts == freezed
          ? _value.availableSorts
          : availableSorts // ignore: cast_nullable_to_non_nullable
              as dynamic,
      filters: filters == freezed
          ? _value.filters
          : filters // ignore: cast_nullable_to_non_nullable
              as dynamic,
      availableFilters: availableFilters == freezed
          ? _value.availableFilters
          : availableFilters // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SearchResult implements _SearchResult {
  const _$_SearchResult(
      {this.siteId,
      this.countryDefaultTimeZone,
      this.query,
      this.paging,
      this.results,
      this.sort,
      this.availableSorts,
      this.filters,
      this.availableFilters});

  factory _$_SearchResult.fromJson(Map<String, dynamic> json) =>
      _$$_SearchResultFromJson(json);

  @override
  final String? siteId;
  @override
  final dynamic countryDefaultTimeZone;
  @override
  final String? query;
  @override
  final dynamic paging;
  @override
  final List<Result>? results;
  @override
  final dynamic sort;
  @override
  final dynamic availableSorts;
  @override
  final dynamic filters;
  @override
  final dynamic availableFilters;

  @override
  String toString() {
    return 'SearchResult(siteId: $siteId, countryDefaultTimeZone: $countryDefaultTimeZone, query: $query, paging: $paging, results: $results, sort: $sort, availableSorts: $availableSorts, filters: $filters, availableFilters: $availableFilters)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _SearchResult &&
            const DeepCollectionEquality().equals(other.siteId, siteId) &&
            const DeepCollectionEquality()
                .equals(other.countryDefaultTimeZone, countryDefaultTimeZone) &&
            const DeepCollectionEquality().equals(other.query, query) &&
            const DeepCollectionEquality().equals(other.paging, paging) &&
            const DeepCollectionEquality().equals(other.results, results) &&
            const DeepCollectionEquality().equals(other.sort, sort) &&
            const DeepCollectionEquality()
                .equals(other.availableSorts, availableSorts) &&
            const DeepCollectionEquality().equals(other.filters, filters) &&
            const DeepCollectionEquality()
                .equals(other.availableFilters, availableFilters));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(siteId),
      const DeepCollectionEquality().hash(countryDefaultTimeZone),
      const DeepCollectionEquality().hash(query),
      const DeepCollectionEquality().hash(paging),
      const DeepCollectionEquality().hash(results),
      const DeepCollectionEquality().hash(sort),
      const DeepCollectionEquality().hash(availableSorts),
      const DeepCollectionEquality().hash(filters),
      const DeepCollectionEquality().hash(availableFilters));

  @JsonKey(ignore: true)
  @override
  _$SearchResultCopyWith<_SearchResult> get copyWith =>
      __$SearchResultCopyWithImpl<_SearchResult>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SearchResultToJson(this);
  }
}

abstract class _SearchResult implements SearchResult {
  const factory _SearchResult(
      {String? siteId,
      dynamic countryDefaultTimeZone,
      String? query,
      dynamic paging,
      List<Result>? results,
      dynamic sort,
      dynamic availableSorts,
      dynamic filters,
      dynamic availableFilters}) = _$_SearchResult;

  factory _SearchResult.fromJson(Map<String, dynamic> json) =
      _$_SearchResult.fromJson;

  @override
  String? get siteId;
  @override
  dynamic get countryDefaultTimeZone;
  @override
  String? get query;
  @override
  dynamic get paging;
  @override
  List<Result>? get results;
  @override
  dynamic get sort;
  @override
  dynamic get availableSorts;
  @override
  dynamic get filters;
  @override
  dynamic get availableFilters;
  @override
  @JsonKey(ignore: true)
  _$SearchResultCopyWith<_SearchResult> get copyWith =>
      throw _privateConstructorUsedError;
}
