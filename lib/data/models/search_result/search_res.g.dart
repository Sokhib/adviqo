// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_res.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SearchResult _$$_SearchResultFromJson(Map<String, dynamic> json) =>
    _$_SearchResult(
      siteId: json['siteId'] as String?,
      countryDefaultTimeZone: json['countryDefaultTimeZone'],
      query: json['query'] as String?,
      paging: json['paging'],
      results: (json['results'] as List<dynamic>?)
          ?.map((e) => Result.fromJson(e as Map<String, dynamic>))
          .toList(),
      sort: json['sort'],
      availableSorts: json['availableSorts'],
      filters: json['filters'],
      availableFilters: json['availableFilters'],
    );

Map<String, dynamic> _$$_SearchResultToJson(_$_SearchResult instance) =>
    <String, dynamic>{
      'siteId': instance.siteId,
      'countryDefaultTimeZone': instance.countryDefaultTimeZone,
      'query': instance.query,
      'paging': instance.paging,
      'results': instance.results,
      'sort': instance.sort,
      'availableSorts': instance.availableSorts,
      'filters': instance.filters,
      'availableFilters': instance.availableFilters,
    };
