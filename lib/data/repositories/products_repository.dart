import 'dart:convert';
import 'dart:developer';

import 'package:adviqo/data/models/product/description/description.dart';
import 'package:adviqo/data/models/product/product.dart';
import 'package:adviqo/data/models/search_result/result/result.dart';
import 'package:adviqo/data/models/search_result/search_res.dart';
import 'package:adviqo/logic/models/failure_model.dart';
import 'package:adviqo/logic/models/parsed_item_models.dart';
import 'package:adviqo/logic/models/product_model.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart';

class ProductsRepository {
  Future<Either<List<ParsedItemModel>, FailureModel>> getSearchingProduct(String name) async {
    String rootLink = 'https://api.mercadolibre.com';
    String helperLink = '/sites/MLU/search?q=';
    String type = '#json';

    try {
      Response searchResult = await get(Uri.parse('$rootLink$helperLink$name$type'));
      SearchResult parsedResult = SearchResult.fromJson(jsonDecode(searchResult.body));
      if (parsedResult.results != null) {
        if (parsedResult.results!.isEmpty) {
          return right(FailureModel.noItemToShow('No Items to show'));
        } else {
          List<ParsedItemModel> productList = _getProductList(parsedResult.results!);
          return left(productList);
        }
      } else {
        return right(FailureModel.server('Something wrong happened'));
      }
    } catch (e) {
      return right(FailureModel.application(e.toString()));
    }
  }

  List<ParsedItemModel> _getProductList(List<Result> results) {
    List<ParsedItemModel> productList = [];
    for (var item in results) {
      productList.add(
        ParsedItemModel(
          id: item.id!,
          imagePath: item.thumbnail ?? '',
          title: item.title ?? 'No title',
          price: '${item.prices?.presentation.display_currency} ${item.price}',
        ),
      );
    }
    return productList;
  }

  Future<Either<ProductModel, FailureModel>> getProdectById(String id) async {
    String rootLink = 'https://api.mercadolibre.com';
    String helperLink = '/items/';
    String type = '#json';
    try {
      Response searchResponse = await get(Uri.parse('$rootLink$helperLink$id$type'));
      Response descriptionResponse = await get(Uri.parse('$rootLink$helperLink$id/description$type'));

      Product product = Product.fromJson(jsonDecode(searchResponse.body));
      Description description = Description.fromJson(jsonDecode(descriptionResponse.body));

      List<String> imageLinks = _getPicturesList(product);
      ProductModel productModel = _getConvertedProduct(product, imageLinks, description);

      log(productModel.toString());

      return left(productModel);
    } catch (e) {
      // log(e.toString());
      return right(FailureModel.application(e.toString()));
    }
  }

  List<String> _getPicturesList(Product product) {
    List<String> imageLinks = [];
    if (product.pictures != null) {
      for (var item in product.pictures!) {
        item.url != null ? imageLinks.add(item.url!) : null;
      }
    }
    return imageLinks;
  }

  ProductModel _getConvertedProduct(
    Product product,
    List<String> imageLinks,
    Description description,
  ) {
    return ProductModel(
      id: product.id!,
      title: product.title,
      price: "${product.currencyId} ${product.price}",
      sold: product.sold_quantity,
      left: product.initial_quantity,
      available: product.available_quantity,
      imageLinks: imageLinks,
      description: description.plain_text,
    );
  }
}
