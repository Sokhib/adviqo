import 'package:adviqo/data/repositories/products_repository.dart';
import 'package:adviqo/logic/models/failure_model.dart';
import 'package:adviqo/logic/models/parsed_item_models.dart';
import 'package:adviqo/logic/models/product_model.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'mercadolibre_event.dart';
part 'mercadolibre_state.dart';
part 'mercadolibre_bloc.freezed.dart';

class MercadolibreBloc extends Bloc<MercadolibreEvent, MercadolibreState> {
  List<ProductModel> productsList = [];

  MercadolibreBloc() : super(const _Initial()) {
    ProductsRepository repository = ProductsRepository();

    on<MercadolibreEvent>((event, emit) {});

    on<FetchSearchResultList>(
      (event, emit) async {
        emit(const FetchingSearchList());
        await repository.getSearchingProduct(event.name).then(
              (value) => value.fold(
                (l) => emit(FetchingSearchListSuccess(l)),
                (r) => emit(FetchingSearchListFailure(r)),
              ),
            );
      },
    );

    on<AddToRecent>(
      (event, emit) async {
        addNewProductToList(event.productModel);
      },
    );

    on<FetchItemById>(
      (event, emit) async {
        emit(const FetchingProduct());
        await repository.getProdectById(event.id).then(
              (value) => value.fold(
                (l) => emit(FetchingProductSuccess(l)),
                (r) => emit(FetchingProductFailure(r)),
              ),
            );
      },
    );
  }

  void addNewProductToList(ProductModel l) {
    if (!productsList.contains(l)) {
      for (var item in productsList) {
        if (item.id == l.id) return;
      }
      if (productsList.length > 5) {
        productsList.removeAt(0);
      }
      productsList.add(l);
    }
  }
}
