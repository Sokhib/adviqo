// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'mercadolibre_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$MercadolibreEventTearOff {
  const _$MercadolibreEventTearOff();

  _Started started() {
    return const _Started();
  }

  FetchSearchResultList fetchSearchResultList(String name) {
    return FetchSearchResultList(
      name,
    );
  }

  FetchItemById fetchItemById(String id) {
    return FetchItemById(
      id,
    );
  }

  AddToRecent addToRecent(ProductModel productModel, String id) {
    return AddToRecent(
      productModel,
      id,
    );
  }
}

/// @nodoc
const $MercadolibreEvent = _$MercadolibreEventTearOff();

/// @nodoc
mixin _$MercadolibreEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String name) fetchSearchResultList,
    required TResult Function(String id) fetchItemById,
    required TResult Function(ProductModel productModel, String id) addToRecent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String name)? fetchSearchResultList,
    TResult Function(String id)? fetchItemById,
    TResult Function(ProductModel productModel, String id)? addToRecent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String name)? fetchSearchResultList,
    TResult Function(String id)? fetchItemById,
    TResult Function(ProductModel productModel, String id)? addToRecent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(FetchSearchResultList value)
        fetchSearchResultList,
    required TResult Function(FetchItemById value) fetchItemById,
    required TResult Function(AddToRecent value) addToRecent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(FetchSearchResultList value)? fetchSearchResultList,
    TResult Function(FetchItemById value)? fetchItemById,
    TResult Function(AddToRecent value)? addToRecent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(FetchSearchResultList value)? fetchSearchResultList,
    TResult Function(FetchItemById value)? fetchItemById,
    TResult Function(AddToRecent value)? addToRecent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MercadolibreEventCopyWith<$Res> {
  factory $MercadolibreEventCopyWith(
          MercadolibreEvent value, $Res Function(MercadolibreEvent) then) =
      _$MercadolibreEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$MercadolibreEventCopyWithImpl<$Res>
    implements $MercadolibreEventCopyWith<$Res> {
  _$MercadolibreEventCopyWithImpl(this._value, this._then);

  final MercadolibreEvent _value;
  // ignore: unused_field
  final $Res Function(MercadolibreEvent) _then;
}

/// @nodoc
abstract class _$StartedCopyWith<$Res> {
  factory _$StartedCopyWith(_Started value, $Res Function(_Started) then) =
      __$StartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$StartedCopyWithImpl<$Res> extends _$MercadolibreEventCopyWithImpl<$Res>
    implements _$StartedCopyWith<$Res> {
  __$StartedCopyWithImpl(_Started _value, $Res Function(_Started) _then)
      : super(_value, (v) => _then(v as _Started));

  @override
  _Started get _value => super._value as _Started;
}

/// @nodoc

class _$_Started implements _Started {
  const _$_Started();

  @override
  String toString() {
    return 'MercadolibreEvent.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _Started);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String name) fetchSearchResultList,
    required TResult Function(String id) fetchItemById,
    required TResult Function(ProductModel productModel, String id) addToRecent,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String name)? fetchSearchResultList,
    TResult Function(String id)? fetchItemById,
    TResult Function(ProductModel productModel, String id)? addToRecent,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String name)? fetchSearchResultList,
    TResult Function(String id)? fetchItemById,
    TResult Function(ProductModel productModel, String id)? addToRecent,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(FetchSearchResultList value)
        fetchSearchResultList,
    required TResult Function(FetchItemById value) fetchItemById,
    required TResult Function(AddToRecent value) addToRecent,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(FetchSearchResultList value)? fetchSearchResultList,
    TResult Function(FetchItemById value)? fetchItemById,
    TResult Function(AddToRecent value)? addToRecent,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(FetchSearchResultList value)? fetchSearchResultList,
    TResult Function(FetchItemById value)? fetchItemById,
    TResult Function(AddToRecent value)? addToRecent,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements MercadolibreEvent {
  const factory _Started() = _$_Started;
}

/// @nodoc
abstract class $FetchSearchResultListCopyWith<$Res> {
  factory $FetchSearchResultListCopyWith(FetchSearchResultList value,
          $Res Function(FetchSearchResultList) then) =
      _$FetchSearchResultListCopyWithImpl<$Res>;
  $Res call({String name});
}

/// @nodoc
class _$FetchSearchResultListCopyWithImpl<$Res>
    extends _$MercadolibreEventCopyWithImpl<$Res>
    implements $FetchSearchResultListCopyWith<$Res> {
  _$FetchSearchResultListCopyWithImpl(
      FetchSearchResultList _value, $Res Function(FetchSearchResultList) _then)
      : super(_value, (v) => _then(v as FetchSearchResultList));

  @override
  FetchSearchResultList get _value => super._value as FetchSearchResultList;

  @override
  $Res call({
    Object? name = freezed,
  }) {
    return _then(FetchSearchResultList(
      name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FetchSearchResultList implements FetchSearchResultList {
  const _$FetchSearchResultList(this.name);

  @override
  final String name;

  @override
  String toString() {
    return 'MercadolibreEvent.fetchSearchResultList(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FetchSearchResultList &&
            const DeepCollectionEquality().equals(other.name, name));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(name));

  @JsonKey(ignore: true)
  @override
  $FetchSearchResultListCopyWith<FetchSearchResultList> get copyWith =>
      _$FetchSearchResultListCopyWithImpl<FetchSearchResultList>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String name) fetchSearchResultList,
    required TResult Function(String id) fetchItemById,
    required TResult Function(ProductModel productModel, String id) addToRecent,
  }) {
    return fetchSearchResultList(name);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String name)? fetchSearchResultList,
    TResult Function(String id)? fetchItemById,
    TResult Function(ProductModel productModel, String id)? addToRecent,
  }) {
    return fetchSearchResultList?.call(name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String name)? fetchSearchResultList,
    TResult Function(String id)? fetchItemById,
    TResult Function(ProductModel productModel, String id)? addToRecent,
    required TResult orElse(),
  }) {
    if (fetchSearchResultList != null) {
      return fetchSearchResultList(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(FetchSearchResultList value)
        fetchSearchResultList,
    required TResult Function(FetchItemById value) fetchItemById,
    required TResult Function(AddToRecent value) addToRecent,
  }) {
    return fetchSearchResultList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(FetchSearchResultList value)? fetchSearchResultList,
    TResult Function(FetchItemById value)? fetchItemById,
    TResult Function(AddToRecent value)? addToRecent,
  }) {
    return fetchSearchResultList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(FetchSearchResultList value)? fetchSearchResultList,
    TResult Function(FetchItemById value)? fetchItemById,
    TResult Function(AddToRecent value)? addToRecent,
    required TResult orElse(),
  }) {
    if (fetchSearchResultList != null) {
      return fetchSearchResultList(this);
    }
    return orElse();
  }
}

abstract class FetchSearchResultList implements MercadolibreEvent {
  const factory FetchSearchResultList(String name) = _$FetchSearchResultList;

  String get name;
  @JsonKey(ignore: true)
  $FetchSearchResultListCopyWith<FetchSearchResultList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FetchItemByIdCopyWith<$Res> {
  factory $FetchItemByIdCopyWith(
          FetchItemById value, $Res Function(FetchItemById) then) =
      _$FetchItemByIdCopyWithImpl<$Res>;
  $Res call({String id});
}

/// @nodoc
class _$FetchItemByIdCopyWithImpl<$Res>
    extends _$MercadolibreEventCopyWithImpl<$Res>
    implements $FetchItemByIdCopyWith<$Res> {
  _$FetchItemByIdCopyWithImpl(
      FetchItemById _value, $Res Function(FetchItemById) _then)
      : super(_value, (v) => _then(v as FetchItemById));

  @override
  FetchItemById get _value => super._value as FetchItemById;

  @override
  $Res call({
    Object? id = freezed,
  }) {
    return _then(FetchItemById(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FetchItemById implements FetchItemById {
  const _$FetchItemById(this.id);

  @override
  final String id;

  @override
  String toString() {
    return 'MercadolibreEvent.fetchItemById(id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FetchItemById &&
            const DeepCollectionEquality().equals(other.id, id));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(id));

  @JsonKey(ignore: true)
  @override
  $FetchItemByIdCopyWith<FetchItemById> get copyWith =>
      _$FetchItemByIdCopyWithImpl<FetchItemById>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String name) fetchSearchResultList,
    required TResult Function(String id) fetchItemById,
    required TResult Function(ProductModel productModel, String id) addToRecent,
  }) {
    return fetchItemById(id);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String name)? fetchSearchResultList,
    TResult Function(String id)? fetchItemById,
    TResult Function(ProductModel productModel, String id)? addToRecent,
  }) {
    return fetchItemById?.call(id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String name)? fetchSearchResultList,
    TResult Function(String id)? fetchItemById,
    TResult Function(ProductModel productModel, String id)? addToRecent,
    required TResult orElse(),
  }) {
    if (fetchItemById != null) {
      return fetchItemById(id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(FetchSearchResultList value)
        fetchSearchResultList,
    required TResult Function(FetchItemById value) fetchItemById,
    required TResult Function(AddToRecent value) addToRecent,
  }) {
    return fetchItemById(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(FetchSearchResultList value)? fetchSearchResultList,
    TResult Function(FetchItemById value)? fetchItemById,
    TResult Function(AddToRecent value)? addToRecent,
  }) {
    return fetchItemById?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(FetchSearchResultList value)? fetchSearchResultList,
    TResult Function(FetchItemById value)? fetchItemById,
    TResult Function(AddToRecent value)? addToRecent,
    required TResult orElse(),
  }) {
    if (fetchItemById != null) {
      return fetchItemById(this);
    }
    return orElse();
  }
}

abstract class FetchItemById implements MercadolibreEvent {
  const factory FetchItemById(String id) = _$FetchItemById;

  String get id;
  @JsonKey(ignore: true)
  $FetchItemByIdCopyWith<FetchItemById> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddToRecentCopyWith<$Res> {
  factory $AddToRecentCopyWith(
          AddToRecent value, $Res Function(AddToRecent) then) =
      _$AddToRecentCopyWithImpl<$Res>;
  $Res call({ProductModel productModel, String id});
}

/// @nodoc
class _$AddToRecentCopyWithImpl<$Res>
    extends _$MercadolibreEventCopyWithImpl<$Res>
    implements $AddToRecentCopyWith<$Res> {
  _$AddToRecentCopyWithImpl(
      AddToRecent _value, $Res Function(AddToRecent) _then)
      : super(_value, (v) => _then(v as AddToRecent));

  @override
  AddToRecent get _value => super._value as AddToRecent;

  @override
  $Res call({
    Object? productModel = freezed,
    Object? id = freezed,
  }) {
    return _then(AddToRecent(
      productModel == freezed
          ? _value.productModel
          : productModel // ignore: cast_nullable_to_non_nullable
              as ProductModel,
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$AddToRecent implements AddToRecent {
  const _$AddToRecent(this.productModel, this.id);

  @override
  final ProductModel productModel;
  @override
  final String id;

  @override
  String toString() {
    return 'MercadolibreEvent.addToRecent(productModel: $productModel, id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is AddToRecent &&
            const DeepCollectionEquality()
                .equals(other.productModel, productModel) &&
            const DeepCollectionEquality().equals(other.id, id));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(productModel),
      const DeepCollectionEquality().hash(id));

  @JsonKey(ignore: true)
  @override
  $AddToRecentCopyWith<AddToRecent> get copyWith =>
      _$AddToRecentCopyWithImpl<AddToRecent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String name) fetchSearchResultList,
    required TResult Function(String id) fetchItemById,
    required TResult Function(ProductModel productModel, String id) addToRecent,
  }) {
    return addToRecent(productModel, id);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String name)? fetchSearchResultList,
    TResult Function(String id)? fetchItemById,
    TResult Function(ProductModel productModel, String id)? addToRecent,
  }) {
    return addToRecent?.call(productModel, id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String name)? fetchSearchResultList,
    TResult Function(String id)? fetchItemById,
    TResult Function(ProductModel productModel, String id)? addToRecent,
    required TResult orElse(),
  }) {
    if (addToRecent != null) {
      return addToRecent(productModel, id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(FetchSearchResultList value)
        fetchSearchResultList,
    required TResult Function(FetchItemById value) fetchItemById,
    required TResult Function(AddToRecent value) addToRecent,
  }) {
    return addToRecent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(FetchSearchResultList value)? fetchSearchResultList,
    TResult Function(FetchItemById value)? fetchItemById,
    TResult Function(AddToRecent value)? addToRecent,
  }) {
    return addToRecent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(FetchSearchResultList value)? fetchSearchResultList,
    TResult Function(FetchItemById value)? fetchItemById,
    TResult Function(AddToRecent value)? addToRecent,
    required TResult orElse(),
  }) {
    if (addToRecent != null) {
      return addToRecent(this);
    }
    return orElse();
  }
}

abstract class AddToRecent implements MercadolibreEvent {
  const factory AddToRecent(ProductModel productModel, String id) =
      _$AddToRecent;

  ProductModel get productModel;
  String get id;
  @JsonKey(ignore: true)
  $AddToRecentCopyWith<AddToRecent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
class _$MercadolibreStateTearOff {
  const _$MercadolibreStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  FetchingSearchList fetchingSearchList() {
    return const FetchingSearchList();
  }

  FetchingSearchListFailure fetchingSearchListFailure(FailureModel failure) {
    return FetchingSearchListFailure(
      failure,
    );
  }

  FetchingSearchListSuccess fetchingSearchListSuccess(
      List<ParsedItemModel> parsedItemModel) {
    return FetchingSearchListSuccess(
      parsedItemModel,
    );
  }

  FetchingProduct fetchingProduct() {
    return const FetchingProduct();
  }

  FetchingProductFailure fetchingProductFailure(FailureModel failure) {
    return FetchingProductFailure(
      failure,
    );
  }

  FetchingProductSuccess fetchingProductSuccess(ProductModel product) {
    return FetchingProductSuccess(
      product,
    );
  }
}

/// @nodoc
const $MercadolibreState = _$MercadolibreStateTearOff();

/// @nodoc
mixin _$MercadolibreState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingSearchList,
    required TResult Function(FailureModel failure) fetchingSearchListFailure,
    required TResult Function(List<ParsedItemModel> parsedItemModel)
        fetchingSearchListSuccess,
    required TResult Function() fetchingProduct,
    required TResult Function(FailureModel failure) fetchingProductFailure,
    required TResult Function(ProductModel product) fetchingProductSuccess,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FetchingSearchList value) fetchingSearchList,
    required TResult Function(FetchingSearchListFailure value)
        fetchingSearchListFailure,
    required TResult Function(FetchingSearchListSuccess value)
        fetchingSearchListSuccess,
    required TResult Function(FetchingProduct value) fetchingProduct,
    required TResult Function(FetchingProductFailure value)
        fetchingProductFailure,
    required TResult Function(FetchingProductSuccess value)
        fetchingProductSuccess,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MercadolibreStateCopyWith<$Res> {
  factory $MercadolibreStateCopyWith(
          MercadolibreState value, $Res Function(MercadolibreState) then) =
      _$MercadolibreStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$MercadolibreStateCopyWithImpl<$Res>
    implements $MercadolibreStateCopyWith<$Res> {
  _$MercadolibreStateCopyWithImpl(this._value, this._then);

  final MercadolibreState _value;
  // ignore: unused_field
  final $Res Function(MercadolibreState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$MercadolibreStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'MercadolibreState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingSearchList,
    required TResult Function(FailureModel failure) fetchingSearchListFailure,
    required TResult Function(List<ParsedItemModel> parsedItemModel)
        fetchingSearchListSuccess,
    required TResult Function() fetchingProduct,
    required TResult Function(FailureModel failure) fetchingProductFailure,
    required TResult Function(ProductModel product) fetchingProductSuccess,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FetchingSearchList value) fetchingSearchList,
    required TResult Function(FetchingSearchListFailure value)
        fetchingSearchListFailure,
    required TResult Function(FetchingSearchListSuccess value)
        fetchingSearchListSuccess,
    required TResult Function(FetchingProduct value) fetchingProduct,
    required TResult Function(FetchingProductFailure value)
        fetchingProductFailure,
    required TResult Function(FetchingProductSuccess value)
        fetchingProductSuccess,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements MercadolibreState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class $FetchingSearchListCopyWith<$Res> {
  factory $FetchingSearchListCopyWith(
          FetchingSearchList value, $Res Function(FetchingSearchList) then) =
      _$FetchingSearchListCopyWithImpl<$Res>;
}

/// @nodoc
class _$FetchingSearchListCopyWithImpl<$Res>
    extends _$MercadolibreStateCopyWithImpl<$Res>
    implements $FetchingSearchListCopyWith<$Res> {
  _$FetchingSearchListCopyWithImpl(
      FetchingSearchList _value, $Res Function(FetchingSearchList) _then)
      : super(_value, (v) => _then(v as FetchingSearchList));

  @override
  FetchingSearchList get _value => super._value as FetchingSearchList;
}

/// @nodoc

class _$FetchingSearchList implements FetchingSearchList {
  const _$FetchingSearchList();

  @override
  String toString() {
    return 'MercadolibreState.fetchingSearchList()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is FetchingSearchList);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingSearchList,
    required TResult Function(FailureModel failure) fetchingSearchListFailure,
    required TResult Function(List<ParsedItemModel> parsedItemModel)
        fetchingSearchListSuccess,
    required TResult Function() fetchingProduct,
    required TResult Function(FailureModel failure) fetchingProductFailure,
    required TResult Function(ProductModel product) fetchingProductSuccess,
  }) {
    return fetchingSearchList();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
  }) {
    return fetchingSearchList?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingSearchList != null) {
      return fetchingSearchList();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FetchingSearchList value) fetchingSearchList,
    required TResult Function(FetchingSearchListFailure value)
        fetchingSearchListFailure,
    required TResult Function(FetchingSearchListSuccess value)
        fetchingSearchListSuccess,
    required TResult Function(FetchingProduct value) fetchingProduct,
    required TResult Function(FetchingProductFailure value)
        fetchingProductFailure,
    required TResult Function(FetchingProductSuccess value)
        fetchingProductSuccess,
  }) {
    return fetchingSearchList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
  }) {
    return fetchingSearchList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingSearchList != null) {
      return fetchingSearchList(this);
    }
    return orElse();
  }
}

abstract class FetchingSearchList implements MercadolibreState {
  const factory FetchingSearchList() = _$FetchingSearchList;
}

/// @nodoc
abstract class $FetchingSearchListFailureCopyWith<$Res> {
  factory $FetchingSearchListFailureCopyWith(FetchingSearchListFailure value,
          $Res Function(FetchingSearchListFailure) then) =
      _$FetchingSearchListFailureCopyWithImpl<$Res>;
  $Res call({FailureModel failure});
}

/// @nodoc
class _$FetchingSearchListFailureCopyWithImpl<$Res>
    extends _$MercadolibreStateCopyWithImpl<$Res>
    implements $FetchingSearchListFailureCopyWith<$Res> {
  _$FetchingSearchListFailureCopyWithImpl(FetchingSearchListFailure _value,
      $Res Function(FetchingSearchListFailure) _then)
      : super(_value, (v) => _then(v as FetchingSearchListFailure));

  @override
  FetchingSearchListFailure get _value =>
      super._value as FetchingSearchListFailure;

  @override
  $Res call({
    Object? failure = freezed,
  }) {
    return _then(FetchingSearchListFailure(
      failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as FailureModel,
    ));
  }
}

/// @nodoc

class _$FetchingSearchListFailure implements FetchingSearchListFailure {
  const _$FetchingSearchListFailure(this.failure);

  @override
  final FailureModel failure;

  @override
  String toString() {
    return 'MercadolibreState.fetchingSearchListFailure(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FetchingSearchListFailure &&
            const DeepCollectionEquality().equals(other.failure, failure));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(failure));

  @JsonKey(ignore: true)
  @override
  $FetchingSearchListFailureCopyWith<FetchingSearchListFailure> get copyWith =>
      _$FetchingSearchListFailureCopyWithImpl<FetchingSearchListFailure>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingSearchList,
    required TResult Function(FailureModel failure) fetchingSearchListFailure,
    required TResult Function(List<ParsedItemModel> parsedItemModel)
        fetchingSearchListSuccess,
    required TResult Function() fetchingProduct,
    required TResult Function(FailureModel failure) fetchingProductFailure,
    required TResult Function(ProductModel product) fetchingProductSuccess,
  }) {
    return fetchingSearchListFailure(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
  }) {
    return fetchingSearchListFailure?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingSearchListFailure != null) {
      return fetchingSearchListFailure(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FetchingSearchList value) fetchingSearchList,
    required TResult Function(FetchingSearchListFailure value)
        fetchingSearchListFailure,
    required TResult Function(FetchingSearchListSuccess value)
        fetchingSearchListSuccess,
    required TResult Function(FetchingProduct value) fetchingProduct,
    required TResult Function(FetchingProductFailure value)
        fetchingProductFailure,
    required TResult Function(FetchingProductSuccess value)
        fetchingProductSuccess,
  }) {
    return fetchingSearchListFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
  }) {
    return fetchingSearchListFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingSearchListFailure != null) {
      return fetchingSearchListFailure(this);
    }
    return orElse();
  }
}

abstract class FetchingSearchListFailure implements MercadolibreState {
  const factory FetchingSearchListFailure(FailureModel failure) =
      _$FetchingSearchListFailure;

  FailureModel get failure;
  @JsonKey(ignore: true)
  $FetchingSearchListFailureCopyWith<FetchingSearchListFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FetchingSearchListSuccessCopyWith<$Res> {
  factory $FetchingSearchListSuccessCopyWith(FetchingSearchListSuccess value,
          $Res Function(FetchingSearchListSuccess) then) =
      _$FetchingSearchListSuccessCopyWithImpl<$Res>;
  $Res call({List<ParsedItemModel> parsedItemModel});
}

/// @nodoc
class _$FetchingSearchListSuccessCopyWithImpl<$Res>
    extends _$MercadolibreStateCopyWithImpl<$Res>
    implements $FetchingSearchListSuccessCopyWith<$Res> {
  _$FetchingSearchListSuccessCopyWithImpl(FetchingSearchListSuccess _value,
      $Res Function(FetchingSearchListSuccess) _then)
      : super(_value, (v) => _then(v as FetchingSearchListSuccess));

  @override
  FetchingSearchListSuccess get _value =>
      super._value as FetchingSearchListSuccess;

  @override
  $Res call({
    Object? parsedItemModel = freezed,
  }) {
    return _then(FetchingSearchListSuccess(
      parsedItemModel == freezed
          ? _value.parsedItemModel
          : parsedItemModel // ignore: cast_nullable_to_non_nullable
              as List<ParsedItemModel>,
    ));
  }
}

/// @nodoc

class _$FetchingSearchListSuccess implements FetchingSearchListSuccess {
  const _$FetchingSearchListSuccess(this.parsedItemModel);

  @override
  final List<ParsedItemModel> parsedItemModel;

  @override
  String toString() {
    return 'MercadolibreState.fetchingSearchListSuccess(parsedItemModel: $parsedItemModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FetchingSearchListSuccess &&
            const DeepCollectionEquality()
                .equals(other.parsedItemModel, parsedItemModel));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(parsedItemModel));

  @JsonKey(ignore: true)
  @override
  $FetchingSearchListSuccessCopyWith<FetchingSearchListSuccess> get copyWith =>
      _$FetchingSearchListSuccessCopyWithImpl<FetchingSearchListSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingSearchList,
    required TResult Function(FailureModel failure) fetchingSearchListFailure,
    required TResult Function(List<ParsedItemModel> parsedItemModel)
        fetchingSearchListSuccess,
    required TResult Function() fetchingProduct,
    required TResult Function(FailureModel failure) fetchingProductFailure,
    required TResult Function(ProductModel product) fetchingProductSuccess,
  }) {
    return fetchingSearchListSuccess(parsedItemModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
  }) {
    return fetchingSearchListSuccess?.call(parsedItemModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingSearchListSuccess != null) {
      return fetchingSearchListSuccess(parsedItemModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FetchingSearchList value) fetchingSearchList,
    required TResult Function(FetchingSearchListFailure value)
        fetchingSearchListFailure,
    required TResult Function(FetchingSearchListSuccess value)
        fetchingSearchListSuccess,
    required TResult Function(FetchingProduct value) fetchingProduct,
    required TResult Function(FetchingProductFailure value)
        fetchingProductFailure,
    required TResult Function(FetchingProductSuccess value)
        fetchingProductSuccess,
  }) {
    return fetchingSearchListSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
  }) {
    return fetchingSearchListSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingSearchListSuccess != null) {
      return fetchingSearchListSuccess(this);
    }
    return orElse();
  }
}

abstract class FetchingSearchListSuccess implements MercadolibreState {
  const factory FetchingSearchListSuccess(
      List<ParsedItemModel> parsedItemModel) = _$FetchingSearchListSuccess;

  List<ParsedItemModel> get parsedItemModel;
  @JsonKey(ignore: true)
  $FetchingSearchListSuccessCopyWith<FetchingSearchListSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FetchingProductCopyWith<$Res> {
  factory $FetchingProductCopyWith(
          FetchingProduct value, $Res Function(FetchingProduct) then) =
      _$FetchingProductCopyWithImpl<$Res>;
}

/// @nodoc
class _$FetchingProductCopyWithImpl<$Res>
    extends _$MercadolibreStateCopyWithImpl<$Res>
    implements $FetchingProductCopyWith<$Res> {
  _$FetchingProductCopyWithImpl(
      FetchingProduct _value, $Res Function(FetchingProduct) _then)
      : super(_value, (v) => _then(v as FetchingProduct));

  @override
  FetchingProduct get _value => super._value as FetchingProduct;
}

/// @nodoc

class _$FetchingProduct implements FetchingProduct {
  const _$FetchingProduct();

  @override
  String toString() {
    return 'MercadolibreState.fetchingProduct()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is FetchingProduct);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingSearchList,
    required TResult Function(FailureModel failure) fetchingSearchListFailure,
    required TResult Function(List<ParsedItemModel> parsedItemModel)
        fetchingSearchListSuccess,
    required TResult Function() fetchingProduct,
    required TResult Function(FailureModel failure) fetchingProductFailure,
    required TResult Function(ProductModel product) fetchingProductSuccess,
  }) {
    return fetchingProduct();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
  }) {
    return fetchingProduct?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingProduct != null) {
      return fetchingProduct();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FetchingSearchList value) fetchingSearchList,
    required TResult Function(FetchingSearchListFailure value)
        fetchingSearchListFailure,
    required TResult Function(FetchingSearchListSuccess value)
        fetchingSearchListSuccess,
    required TResult Function(FetchingProduct value) fetchingProduct,
    required TResult Function(FetchingProductFailure value)
        fetchingProductFailure,
    required TResult Function(FetchingProductSuccess value)
        fetchingProductSuccess,
  }) {
    return fetchingProduct(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
  }) {
    return fetchingProduct?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingProduct != null) {
      return fetchingProduct(this);
    }
    return orElse();
  }
}

abstract class FetchingProduct implements MercadolibreState {
  const factory FetchingProduct() = _$FetchingProduct;
}

/// @nodoc
abstract class $FetchingProductFailureCopyWith<$Res> {
  factory $FetchingProductFailureCopyWith(FetchingProductFailure value,
          $Res Function(FetchingProductFailure) then) =
      _$FetchingProductFailureCopyWithImpl<$Res>;
  $Res call({FailureModel failure});
}

/// @nodoc
class _$FetchingProductFailureCopyWithImpl<$Res>
    extends _$MercadolibreStateCopyWithImpl<$Res>
    implements $FetchingProductFailureCopyWith<$Res> {
  _$FetchingProductFailureCopyWithImpl(FetchingProductFailure _value,
      $Res Function(FetchingProductFailure) _then)
      : super(_value, (v) => _then(v as FetchingProductFailure));

  @override
  FetchingProductFailure get _value => super._value as FetchingProductFailure;

  @override
  $Res call({
    Object? failure = freezed,
  }) {
    return _then(FetchingProductFailure(
      failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as FailureModel,
    ));
  }
}

/// @nodoc

class _$FetchingProductFailure implements FetchingProductFailure {
  const _$FetchingProductFailure(this.failure);

  @override
  final FailureModel failure;

  @override
  String toString() {
    return 'MercadolibreState.fetchingProductFailure(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FetchingProductFailure &&
            const DeepCollectionEquality().equals(other.failure, failure));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(failure));

  @JsonKey(ignore: true)
  @override
  $FetchingProductFailureCopyWith<FetchingProductFailure> get copyWith =>
      _$FetchingProductFailureCopyWithImpl<FetchingProductFailure>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingSearchList,
    required TResult Function(FailureModel failure) fetchingSearchListFailure,
    required TResult Function(List<ParsedItemModel> parsedItemModel)
        fetchingSearchListSuccess,
    required TResult Function() fetchingProduct,
    required TResult Function(FailureModel failure) fetchingProductFailure,
    required TResult Function(ProductModel product) fetchingProductSuccess,
  }) {
    return fetchingProductFailure(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
  }) {
    return fetchingProductFailure?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingProductFailure != null) {
      return fetchingProductFailure(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FetchingSearchList value) fetchingSearchList,
    required TResult Function(FetchingSearchListFailure value)
        fetchingSearchListFailure,
    required TResult Function(FetchingSearchListSuccess value)
        fetchingSearchListSuccess,
    required TResult Function(FetchingProduct value) fetchingProduct,
    required TResult Function(FetchingProductFailure value)
        fetchingProductFailure,
    required TResult Function(FetchingProductSuccess value)
        fetchingProductSuccess,
  }) {
    return fetchingProductFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
  }) {
    return fetchingProductFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingProductFailure != null) {
      return fetchingProductFailure(this);
    }
    return orElse();
  }
}

abstract class FetchingProductFailure implements MercadolibreState {
  const factory FetchingProductFailure(FailureModel failure) =
      _$FetchingProductFailure;

  FailureModel get failure;
  @JsonKey(ignore: true)
  $FetchingProductFailureCopyWith<FetchingProductFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FetchingProductSuccessCopyWith<$Res> {
  factory $FetchingProductSuccessCopyWith(FetchingProductSuccess value,
          $Res Function(FetchingProductSuccess) then) =
      _$FetchingProductSuccessCopyWithImpl<$Res>;
  $Res call({ProductModel product});
}

/// @nodoc
class _$FetchingProductSuccessCopyWithImpl<$Res>
    extends _$MercadolibreStateCopyWithImpl<$Res>
    implements $FetchingProductSuccessCopyWith<$Res> {
  _$FetchingProductSuccessCopyWithImpl(FetchingProductSuccess _value,
      $Res Function(FetchingProductSuccess) _then)
      : super(_value, (v) => _then(v as FetchingProductSuccess));

  @override
  FetchingProductSuccess get _value => super._value as FetchingProductSuccess;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(FetchingProductSuccess(
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as ProductModel,
    ));
  }
}

/// @nodoc

class _$FetchingProductSuccess implements FetchingProductSuccess {
  const _$FetchingProductSuccess(this.product);

  @override
  final ProductModel product;

  @override
  String toString() {
    return 'MercadolibreState.fetchingProductSuccess(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FetchingProductSuccess &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  $FetchingProductSuccessCopyWith<FetchingProductSuccess> get copyWith =>
      _$FetchingProductSuccessCopyWithImpl<FetchingProductSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingSearchList,
    required TResult Function(FailureModel failure) fetchingSearchListFailure,
    required TResult Function(List<ParsedItemModel> parsedItemModel)
        fetchingSearchListSuccess,
    required TResult Function() fetchingProduct,
    required TResult Function(FailureModel failure) fetchingProductFailure,
    required TResult Function(ProductModel product) fetchingProductSuccess,
  }) {
    return fetchingProductSuccess(product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
  }) {
    return fetchingProductSuccess?.call(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingSearchList,
    TResult Function(FailureModel failure)? fetchingSearchListFailure,
    TResult Function(List<ParsedItemModel> parsedItemModel)?
        fetchingSearchListSuccess,
    TResult Function()? fetchingProduct,
    TResult Function(FailureModel failure)? fetchingProductFailure,
    TResult Function(ProductModel product)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingProductSuccess != null) {
      return fetchingProductSuccess(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FetchingSearchList value) fetchingSearchList,
    required TResult Function(FetchingSearchListFailure value)
        fetchingSearchListFailure,
    required TResult Function(FetchingSearchListSuccess value)
        fetchingSearchListSuccess,
    required TResult Function(FetchingProduct value) fetchingProduct,
    required TResult Function(FetchingProductFailure value)
        fetchingProductFailure,
    required TResult Function(FetchingProductSuccess value)
        fetchingProductSuccess,
  }) {
    return fetchingProductSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
  }) {
    return fetchingProductSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FetchingSearchList value)? fetchingSearchList,
    TResult Function(FetchingSearchListFailure value)?
        fetchingSearchListFailure,
    TResult Function(FetchingSearchListSuccess value)?
        fetchingSearchListSuccess,
    TResult Function(FetchingProduct value)? fetchingProduct,
    TResult Function(FetchingProductFailure value)? fetchingProductFailure,
    TResult Function(FetchingProductSuccess value)? fetchingProductSuccess,
    required TResult orElse(),
  }) {
    if (fetchingProductSuccess != null) {
      return fetchingProductSuccess(this);
    }
    return orElse();
  }
}

abstract class FetchingProductSuccess implements MercadolibreState {
  const factory FetchingProductSuccess(ProductModel product) =
      _$FetchingProductSuccess;

  ProductModel get product;
  @JsonKey(ignore: true)
  $FetchingProductSuccessCopyWith<FetchingProductSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}
