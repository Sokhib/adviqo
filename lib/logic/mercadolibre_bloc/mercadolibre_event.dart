part of 'mercadolibre_bloc.dart';

@freezed
class MercadolibreEvent with _$MercadolibreEvent {
  const factory MercadolibreEvent.started() = _Started;

  const factory MercadolibreEvent.fetchSearchResultList(String name) = FetchSearchResultList;

  const factory MercadolibreEvent.fetchItemById(String id) = FetchItemById;

  const factory MercadolibreEvent.addToRecent(ProductModel productModel, String id) = AddToRecent;
}
