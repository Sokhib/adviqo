part of 'mercadolibre_bloc.dart';

@freezed
class MercadolibreState with _$MercadolibreState {
  const factory MercadolibreState.initial() = _Initial;

  const factory MercadolibreState.fetchingSearchList() = FetchingSearchList;
  const factory MercadolibreState.fetchingSearchListFailure(FailureModel failure) = FetchingSearchListFailure;
  const factory MercadolibreState.fetchingSearchListSuccess(List<ParsedItemModel> parsedItemModel) =
      FetchingSearchListSuccess;

  const factory MercadolibreState.fetchingProduct() = FetchingProduct;
  const factory MercadolibreState.fetchingProductFailure(FailureModel failure) = FetchingProductFailure;
  const factory MercadolibreState.fetchingProductSuccess(ProductModel product) = FetchingProductSuccess;
}
