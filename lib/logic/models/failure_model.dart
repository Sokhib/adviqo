class FailureModel {
  FailureModel({
    this.responseCode = 0,
    required this.errorType,
    required this.message,
  });

  int responseCode;
  ErrorType errorType;
  String message;

  factory FailureModel.timeout() =>
      FailureModel(errorType: ErrorType.applicationError, message: 'No result for searched item');

  factory FailureModel.server(String message) =>
      FailureModel(errorType: ErrorType.serverError, message: message);
  factory FailureModel.application(String message) =>
      FailureModel(errorType: ErrorType.applicationError, message: message);
  factory FailureModel.noItemToShow(String message) =>
      FailureModel(errorType: ErrorType.noItemToShow, message: message);
}

enum ErrorType { serverError, applicationError, noItemToShow }
