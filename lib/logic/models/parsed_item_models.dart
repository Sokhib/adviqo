class ParsedItemModel {
  String imagePath;
  String title;
  String price;
  String id;

  ParsedItemModel({
    required this.id,
    required this.imagePath,
    required this.title,
    required this.price,
  });
}
