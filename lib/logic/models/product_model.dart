class ProductModel {
  String id;
  String? title;
  String? price;
  String? description;
  int? sold;
  int? left;
  int? available;
  List<String> imageLinks;

  ProductModel({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.sold,
    required this.left,
    required this.available,
    required this.imageLinks,
  });

  factory ProductModel.mocked() => ProductModel(
        id: 'Loading',
        title: 'Loading...',
        price: 'Loading...',
        sold: 0,
        left: 0,
        imageLinks: [],
        description: '',
        available: 0,
      );

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        id: json['id'] as String,
        title: json['title'] as String,
        description: json['description'] as String,
        price: json['price'] as String,
        sold: json['sold'] as int,
        left: json['left'] as int,
        available: json['available'] as int,
        imageLinks: json['available'] as List<String>,
      );

  Map<String, dynamic> productModelToJson() => {
        'id': id,
        'title': title,
        'description': description,
        'price': price,
        'sold': sold,
        'left': left,
        'available': available,
        'imageLinks': imageLinks
      };
}
