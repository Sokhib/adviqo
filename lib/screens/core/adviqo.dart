import 'dart:developer';

import 'package:adviqo/constants/strings.dart';
import 'package:adviqo/logic/mercadolibre_bloc/mercadolibre_bloc.dart';
import 'package:adviqo/screens/helper/widgets.dart';
import 'package:adviqo/screens/router/auto_router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Adviqo extends StatelessWidget {
  Adviqo({Key? key}) : super(key: key);

  final AutoRoute route = AutoRoute();
  static bool connected = true;

  static bool checkConnection(BuildContext context) {
    if (!connected) {
      showErrorDialog(context, checkYourConnection);
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MercadolibreBloc(),
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        routeInformationParser: route.defaultRouteParser(),
        routerDelegate: route.delegate(),
      ),
    );
  }
}
