import 'package:flutter/material.dart';

const TextStyle blueTextStyle = TextStyle(color: Colors.blue);
const TextStyle blueTextStyle24WithBold =
    TextStyle(color: Colors.blue, fontSize: 24, fontWeight: FontWeight.bold);
const TextStyle blackTextStyle24 = TextStyle(fontWeight: FontWeight.w500, fontSize: 24);
