import 'package:adviqo/constants/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

late BuildContext? loadingDialogContext;

void showLoadingDialog(BuildContext buildContext) {
  showGeneralDialog(
    context: buildContext,
    pageBuilder:
        (BuildContext dialodContext, Animation<double> animation, Animation<double> secondaryAnimation) {
      loadingDialogContext = dialodContext;
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(buildContext).size.width / 2 - 20,
          vertical: MediaQuery.of(buildContext).size.height / 2 - 20,
        ),
        child: const CircularProgressIndicator(
          color: Colors.black,
          strokeWidth: 2,
        ),
        color: Colors.white.withOpacity(0.75),
      );
    },
  );
}

void showErrorDialog(BuildContext buildContext, String message) {
  showDialog(
    context: buildContext,
    builder: (dialodContext) => CupertinoAlertDialog(
      title: const Text(connectionError),
      content: Text(message),
      actions: [
        CupertinoDialogAction(
          child: const Text(ok),
          onPressed: () {
            Navigator.pop(dialodContext);
          },
        )
      ],
    ),
  );
}

void removeLoadingDialog(BuildContext context) {
  try {
    loadingDialogContext != null ? Navigator.pop(context) : null;
    loadingDialogContext = null;
  } catch (e) {}
}
