import 'dart:async';

import 'package:adviqo/constants/strings.dart';
import 'package:adviqo/logic/mercadolibre_bloc/mercadolibre_bloc.dart';
import 'package:adviqo/logic/models/product_model.dart';
import 'package:adviqo/screens/helper/paddings.dart';
import 'package:adviqo/screens/helper/styles.dart';
import 'package:adviqo/screens/helper/widgets.dart';
import 'package:adviqo/screens/router/auto_router.gr.dart';
import 'package:adviqo/screens/search/widgets/announce_card.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ItemDetailesScreen extends StatefulWidget {
  const ItemDetailesScreen({
    Key? key,
    required this.price,
    required this.id,
  }) : super(key: key);

  final String price;
  final String id;

  @override
  State<ItemDetailesScreen> createState() => _ItemDetailesScreenState();
}

class _ItemDetailesScreenState extends State<ItemDetailesScreen> {
  late double swipableCardHeight = MediaQuery.of(context).size.height * 0.4;
  late List<ProductModel> suggestions;

  ProductModel _productModel = ProductModel.mocked();
  StreamController<int> currentImageIndex = StreamController.broadcast();

  @override
  void initState() {
    context.read<MercadolibreBloc>().add(FetchItemById(widget.id));
    suggestions = context.read<MercadolibreBloc>().productsList;
    super.initState();
  }

  void itemClicked(String id, String price) {
    context.router.replace(ItemDetailesRoute(price: price, id: id, key: UniqueKey()));
  }

  void _takeProductToLocal(ProductModel product) {
    _productModel = product;
    _productModel.price = widget.price;
    context.read<MercadolibreBloc>().add(AddToRecent(_productModel, widget.id));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MercadolibreBloc, MercadolibreState>(
      listener: (_, state) {
        state.maybeWhen(
          fetchingProduct: () {
            showLoadingDialog(context);
          },
          fetchingProductSuccess: (product) {
            _takeProductToLocal(product);
            removeLoadingDialog(context);
          },
          orElse: () {
            removeLoadingDialog(context);
          },
        );
      },
      builder: (context, state) {
        return Scaffold(
          body: SafeArea(
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Stack(
                    children: [
                      SizedBox(
                        height: swipableCardHeight,
                        child: PageView(
                          onPageChanged: (index) => currentImageIndex.add(index),
                          scrollDirection: Axis.horizontal,
                          children: _getImagePager(),
                        ),
                      ),
                      Positioned(
                        child: StreamBuilder<int>(
                          stream: currentImageIndex.stream,
                          builder: (context, snapshot) {
                            return _getCurrentIndexOfImagePage(snapshot.data ?? 0);
                          },
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(_productModel.title ?? '', style: blackTextStyle24),
                        height8,
                        Text(widget.price, style: blueTextStyle24WithBold),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('$available ${_productModel.available}', style: blueTextStyle),
                            const SizedBox(width: 24),
                            Text('$left ${_productModel.left}', style: blueTextStyle),
                            const SizedBox(width: 24),
                            Text('$sold ${_productModel.sold}', style: blueTextStyle),
                          ],
                        ),
                        height16,
                        Text(_productModel.description ?? ''),
                        height16,
                        _showRecentlyEnteredText(),
                      ],
                    ),
                  ),
                  SizedBox(
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      child: _cards(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  List<Widget> _getImagePager() => _productModel.imageLinks.map((e) {
        return SizedBox(
          width: double.infinity,
          child: Builder(
            builder: (_) {
              return Image.network(e);
            },
          ),
        );
      }).toList();

  Widget _getCurrentIndexOfImagePage(int i) => Container(
        margin: const EdgeInsets.only(top: 4, left: 4),
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        child: Text("${(i) + 1}/${_productModel.imageLinks.length}"),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: Colors.white.withOpacity(0.8),
        ),
      );

  Widget _showRecentlyEnteredText() {
    return suggestions.isNotEmpty
        ? const Text(
            recentlyEntered,
            style: blackTextStyle24,
          )
        : Container();
  }

  Widget _cards() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: List.generate(
        suggestions.length,
        (index) {
          return _getRecentlyEnteredCard(index);
        },
      ),
    );
  }

  Widget _getRecentlyEnteredCard(int index) => suggestions[index].id != widget.id
      ? ItemCard(
          id: suggestions[index].id,
          price: suggestions[index].price!,
          title: suggestions[index].title!,
          thumbnail: suggestions[index].imageLinks[0],
          itemClicked: itemClicked,
          widthSize: 150,
        )
      : Container();
}
