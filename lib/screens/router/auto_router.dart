import 'package:adviqo/screens/item_detailes/item_detailes.dart';
import 'package:adviqo/screens/search/search_screen.dart';
import 'package:auto_route/auto_route.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Screen,Route',
  routes: <AutoRoute>[
    AutoRoute(page: SearchScreen, path: '/'),
    AutoRoute(page: ItemDetailesScreen),
  ],
)
class $AutoRoute {}
