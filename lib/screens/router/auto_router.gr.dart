// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i3;
import 'package:flutter/material.dart' as _i4;

import '../item_detailes/item_detailes.dart' as _i2;
import '../search/search_screen.dart' as _i1;

class AutoRoute extends _i3.RootStackRouter {
  AutoRoute([_i4.GlobalKey<_i4.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    SearchRoute.name: (routeData) {
      return _i3.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.SearchScreen());
    },
    ItemDetailesRoute.name: (routeData) {
      final args = routeData.argsAs<ItemDetailesRouteArgs>();
      return _i3.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i2.ItemDetailesScreen(
              key: args.key, price: args.price, id: args.id));
    }
  };

  @override
  List<_i3.RouteConfig> get routes => [
        _i3.RouteConfig(SearchRoute.name, path: '/'),
        _i3.RouteConfig(ItemDetailesRoute.name, path: '/item-detailes-screen')
      ];
}

/// generated route for
/// [_i1.SearchScreen]
class SearchRoute extends _i3.PageRouteInfo<void> {
  const SearchRoute() : super(SearchRoute.name, path: '/');

  static const String name = 'SearchRoute';
}

/// generated route for
/// [_i2.ItemDetailesScreen]
class ItemDetailesRoute extends _i3.PageRouteInfo<ItemDetailesRouteArgs> {
  ItemDetailesRoute({_i4.Key? key, required String price, required String id})
      : super(ItemDetailesRoute.name,
            path: '/item-detailes-screen',
            args: ItemDetailesRouteArgs(key: key, price: price, id: id));

  static const String name = 'ItemDetailesRoute';
}

class ItemDetailesRouteArgs {
  const ItemDetailesRouteArgs(
      {this.key, required this.price, required this.id});

  final _i4.Key? key;

  final String price;

  final String id;

  @override
  String toString() {
    return 'ItemDetailesRouteArgs{key: $key, price: $price, id: $id}';
  }
}
