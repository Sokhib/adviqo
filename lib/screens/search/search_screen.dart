import 'dart:developer';

import 'package:adviqo/constants/colors.dart';
import 'package:adviqo/constants/strings.dart';
import 'package:adviqo/logic/mercadolibre_bloc/mercadolibre_bloc.dart';
import 'package:adviqo/logic/models/parsed_item_models.dart';
import 'package:adviqo/screens/core/adviqo.dart';
import 'package:adviqo/screens/helper/widgets.dart';
import 'package:adviqo/screens/router/auto_router.gr.dart';
import 'package:adviqo/screens/search/widgets/announce_card.dart';
import 'package:auto_route/auto_route.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  String searchedValue = '';
  bool failedToLoad = false;
  late FocusNode fn;
  List<ParsedItemModel> parsedItemModel = [];

  @override
  void initState() {
    fn = FocusNode();
    _addListenerToFocuseNode();
    _listenConnectivity();
    super.initState();
  }

  void _addListenerToFocuseNode() {
    fn.addListener(() {
      !fn.hasFocus && searchedValue.isNotEmpty
          ? context.read<MercadolibreBloc>().add(FetchSearchResultList(searchedValue))
          : null;
    });
  }

  void _listenConnectivity() {
    Connectivity().onConnectivityChanged.listen(
      (connectivityResult) {
        log(connectivityResult.name);
        if (connectivityResult == ConnectivityResult.mobile ||
            connectivityResult == ConnectivityResult.wifi) {
          Adviqo.connected = true;
        } else {
          Adviqo.connected = false;
        }
      },
    );
  }

  _searchOnTaped() {
    FocusScope.of(context).unfocus();
    searchedValue.isNotEmpty && (Adviqo.checkConnection(context))
        ? context.read<MercadolibreBloc>().add(FetchSearchResultList(searchedValue))
        : null;
  }

  @override
  void dispose() {
    fn.dispose();
    super.dispose();
  }

  void itemClicked(String id, String price) {
    if (Adviqo.checkConnection(context)) context.router.push(ItemDetailesRoute(price: price, id: id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: appBarBackgorundColor,
        title: TextField(
          focusNode: fn,
          autofocus: false,
          decoration: _inputDecoration(),
          onChanged: (value) => searchedValue = value,
        ),
        actions: [
          Container(
            padding: const EdgeInsets.only(right: 24),
            child: GestureDetector(
              child: const Icon(Icons.search_rounded, color: gray),
              onTap: () => _searchOnTaped(),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              child: BlocConsumer<MercadolibreBloc, MercadolibreState>(
                listener: (_, state) {
                  state.maybeWhen(
                    fetchingSearchList: () {
                      showLoadingDialog(context);
                    },
                    fetchingSearchListFailure: (failure) {
                      parsedItemModel = [];
                      removeLoadingDialog(context);
                      failedToLoad = true;
                    },
                    fetchingSearchListSuccess: (productsList) {
                      parsedItemModel = productsList;
                      removeLoadingDialog(context);
                      failedToLoad = false;
                    },
                    orElse: () {},
                  );
                },
                builder: (_, state) {
                  return parsedItemModel.isNotEmpty
                      ? StaggeredGrid.count(
                          crossAxisCount: 2,
                          children: List.generate(
                            parsedItemModel.length,
                            (index) {
                              return ItemCard(
                                id: parsedItemModel[index].id,
                                price: parsedItemModel[index].price,
                                title: parsedItemModel[index].title,
                                thumbnail: parsedItemModel[index].imagePath,
                                itemClicked: itemClicked,
                              );
                            },
                          ),
                        )
                      : Container(
                          alignment: Alignment.center,
                          child: failedToLoad ? const Text(noResult) : Container(),
                        );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  InputDecoration _inputDecoration() => const InputDecoration(
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        hintText: searchHere,
      );
}
