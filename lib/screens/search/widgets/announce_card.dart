import 'package:adviqo/constants/colors.dart';
import 'package:flutter/material.dart';

class ItemCard extends StatefulWidget {
  const ItemCard({
    Key? key,
    required this.id,
    required this.title,
    required this.price,
    required this.thumbnail,
    required this.itemClicked,
    this.widthSize,
  }) : super(key: key);

  final String id;
  final String title;
  final String price;
  final String thumbnail;
  final Function itemClicked;
  final double? widthSize;

  @override
  State<ItemCard> createState() => _ItemCardState();
}

class _ItemCardState extends State<ItemCard> {
  bool saved = false;
  late double imageHeight = MediaQuery.of(context).size.width / 2.0;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      margin: const EdgeInsets.all(8),
      child: SizedBox(
        width: widget.widthSize,
        child: InkWell(
          onTap: () => widget.itemClicked(widget.id, widget.price),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.network(
                widget.thumbnail,
                width: widget.widthSize == null ? double.infinity : null,
                height: widget.widthSize,
                fit: BoxFit.cover,
              ),
              Container(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(height: 8),
                    Text(
                      widget.title,
                      maxLines: widget.widthSize == null ? 3 : 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    const SizedBox(height: 8),
                    Text(
                      widget.price,
                      maxLines: 1,
                      style: const TextStyle(fontSize: 14, color: darkBlue, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
